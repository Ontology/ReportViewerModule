﻿using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Base;
using OntoMsg_Module.Models;
using ReportViewerModule.Models;
using ReportViewerModule.Notifications;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportViewerModule.Services
{
    public class ServiceAgent_MSSQL : NotifyPropertyChange
    {
        private clsLocalConfig localConfig;

        private object asyncLocker = new object();

        private long lngOffset = 0;

        private List<Dictionary<string, object>> nextRows;
        public List<Dictionary<string, object>> NextRows
        {
            get
            {
                lock(asyncLocker)
                {
                    return nextRows;
                }
                
            }
            set
            {
                lock(asyncLocker)
                {
                    nextRows = value;
                }
                RaisePropertyChanged(NotifyChanges.ServiceMSSQL_NextRows);

            }
        }

        private System.Threading.Thread getNextRowsAsync;

        private object serviceLocker = new object();

        public clsOntologyItem GetData(ReportItem report, List<ReportField> reportFields)
        {
            var threadParam = new ThreadParam
            {
                ConnectionString = "Provider=SQLNCLI11;Data Source=" + report.NameServer + @"\" + localConfig.Globals.Rep_Instance + ";Initial Catalog=" + report.NameDatabaseOrIndex +
                    ";Integrated Security=SSPI",
                Query = "SELECT * FROM [" + report.NameDatabaseOrIndex + "]..[" + report.NameDBViewOrEsType + "]",
                ReportFields = reportFields
            };

            StopReadData();
            getNextRowsAsync = new System.Threading.Thread(GetDataAsync);
            getNextRowsAsync.Start(threadParam);
            nextRows = new List<Dictionary<string, object>>();

            return localConfig.Globals.LState_Success.Clone();
        }

        private async Task<clsOntologyItem> SyncSQLDBRelations(ReportItem report, List<clsClassRel> classRelList, bool doOr = false, bool initialize = true, bool finalize = true)
        {

            var dbReaderClassRel = new OntologyModDBConnector(localConfig.Globals);
            var dbReaderObjectRel = new OntologyModDBConnector(localConfig.Globals);
            var dbReaderClasses = new OntologyModDBConnector(localConfig.Globals);
            var dbReaderRelType = new OntologyModDBConnector(localConfig.Globals);

            var searchObjRel = new List<clsObjectRel>();

            var path = "";
            var path2 = "";
            var classNameLeft = "";
            var classNameRight = "";
            var relationTypeName = "";
            var line = "";

            var ix = 0;

            var result = localConfig.Globals.LState_Success.Clone();
            
            lock (serviceLocker)
            {
                var connectionString = "Data Source=" + report.NameServer + @"\" + localConfig.Globals.Rep_Instance + ";Initial Catalog=" + report.NameDatabaseOrIndex +
                    ";Integrated Security=True";

                var connection = new SqlConnection(connectionString);
                connection.Open();

                if (initialize)
                {
                    var command = new SqlCommand("initialize_Tables", connection);
                    command.Parameters.Add("@Type", SqlDbType.NVarChar).Value = localConfig.Globals.Type_ObjectRel;
                    command.ExecuteNonQuery();
                }

                dbReaderClassRel.GetDataClassRel(classRelList, doIds: false, doOr: doOr);

                foreach (var classRel in dbReaderClassRel.ClassRels)
                {
                    searchObjRel.Clear();
                    searchObjRel.Add(new clsObjectRel
                    {
                        ID_Parent_Object = classRel.ID_Class_Left,
                        ID_Parent_Other = classRel.ID_Class_Right,
                        ID_RelationType = classRel.ID_RelationType
                    });

                    dbReaderObjectRel.GetDataObjectRel(searchObjRel);

                    path = "%Temp%\\" + Guid.NewGuid().ToString() + ".xml";
                    path2 = "%Temp%\\" + Guid.NewGuid().ToString() + ".xml";
                    path = Environment.ExpandEnvironmentVariables(path);
                    path2 = Environment.ExpandEnvironmentVariables(path2);

                    classNameLeft = classRel.Name_Class_Left;
                    classNameRight = classRel.Name_Class_Right;
                    relationTypeName = classRel.Name_RelationType;

                    var command = new SqlCommand("initialize_Table_relT", connection);
                    command.Parameters.Add("@Name_Class_Left", SqlDbType.NVarChar).Value = classNameLeft;
                    command.Parameters.Add("@Name_Class_Right", SqlDbType.NVarChar).Value = classNameRight;
                    command.Parameters.Add("@Name_RelationType", SqlDbType.NVarChar).Value = relationTypeName;
                    command.ExecuteNonQuery();

                    ix = 0;
                    var objRel = dbReaderObjectRel.ObjectRels.OrderBy(obRel => obRel.ID_Parent_Other).ToList();

                    if (objRel.Any())
                    {
                        while (ix < objRel.Count)
                        {
                            var textWriter = new StreamWriter(path, false, System.Text.Encoding.UTF8);
                            var textWriter2 = new StreamWriter(path2, false, System.Text.Encoding.UTF8);

                            line = "<?xml version=\"1.0\" encoding =\"UTF -8\"?>";
                            textWriter.WriteLine(line);
                            textWriter2.WriteLine(line);

                            line = "<root>";

                            textWriter.WriteLine(line);
                            textWriter2.WriteLine(line);

                            for (var jx = ix; jx <= ix + 500; jx++)
                            {
                                if (jx < objRel.Count)
                                {
                                    var objRelItem = objRel[jx];

                                    if ((!string.IsNullOrEmpty(classNameLeft) &&
                                        !string.IsNullOrEmpty(classNameRight) &&
                                        !string.IsNullOrEmpty(relationTypeName) &&
                                        (relationTypeName != objRelItem.Name_RelationType ||
                                         classNameLeft != objRelItem.Name_Parent_Object ||
                                         classNameRight != objRelItem.Name_Parent_Other)))
                                    {
                                        line = "</root>";
                                        textWriter.WriteLine(line);
                                        textWriter2.WriteLine(line);
                                        textWriter.Close();
                                        textWriter2.Close();

                                        command = new SqlCommand("create_Table_relT", connection);
                                        command.Parameters.Add("@Ontology", SqlDbType.NVarChar).Value = localConfig.Globals.Type_ObjectRel;
                                        command.Parameters.Add("@Name_Class_Left", SqlDbType.NVarChar).Value = classNameLeft;
                                        command.Parameters.Add("@Name_Class_Right", SqlDbType.NVarChar).Value = classNameRight;
                                        command.Parameters.Add("@Name_RelationType", SqlDbType.NVarChar).Value = relationTypeName;
                                        command.Parameters.Add("@Path_File", SqlDbType.NVarChar).Value = path;
                                        command.Parameters.Add("@Objects", SqlDbType.Bit).Value = true;
                                        command.ExecuteNonQuery();

                                        command = new SqlCommand("create_Table_relT_OR", connection);
                                        command.Parameters.Add("@Ontology", SqlDbType.NVarChar).Value = localConfig.Globals.Type_ObjectRel;
                                        command.Parameters.Add("@Name_Class_Left", SqlDbType.NVarChar).Value = classNameLeft;
                                        command.Parameters.Add("@Name_RelationType", SqlDbType.NVarChar).Value = relationTypeName;
                                        command.Parameters.Add("@Path_File", SqlDbType.NVarChar).Value = path2;
                                        command.Parameters.Add("@Objects", SqlDbType.Bit).Value = true;
                                        command.ExecuteNonQuery();

                                        textWriter = new StreamWriter(path, false, System.Text.Encoding.UTF8);
                                        textWriter2 = new StreamWriter(path2, false, System.Text.Encoding.UTF8);

                                        line = "<?xml version=\"1.0\" encoding =\"UTF -8\"?>";
                                        textWriter.WriteLine(line);
                                        textWriter2.WriteLine(line);

                                        line = "<root>";

                                        textWriter.WriteLine(line);
                                        textWriter2.WriteLine(line);

                                        classNameLeft = objRelItem.Name_Parent_Object;
                                        classNameRight = objRelItem.Name_Parent_Other;
                                        relationTypeName = objRelItem.Name_RelationType;
                                    }
                                    else
                                    {
                                        classNameLeft = objRelItem.Name_Parent_Object;
                                        classNameRight = objRelItem.Name_Parent_Other;
                                        relationTypeName = objRelItem.Name_RelationType;
                                    }

                                    line = "<tmptbl>";
                                    textWriter.WriteLine(line);
                                    textWriter2.WriteLine(line);

                                    line = "<GUID_Object_Left>" + objRelItem.ID_Object + "</GUID_Object_Left>";
                                    textWriter.WriteLine(line);
                                    textWriter2.WriteLine(line);
                                    line = "<GUID_Object_Right>" + objRelItem.ID_Other + "</GUID_Object_Right>";
                                    textWriter.WriteLine(line);
                                    line = "<GUID_Right>" + objRelItem.ID_Other + "</GUID_Right>";
                                    textWriter2.WriteLine(line);
                                    line = "<Name_Right><![CDATA[" + objRelItem.Name_Other + "]]></Name_Right>";
                                    textWriter2.WriteLine(line);
                                    line = "<GUID_RelationType>" + objRelItem.ID_RelationType + "</GUID_RelationType>";
                                    textWriter.WriteLine(line);
                                    textWriter2.WriteLine(line);
                                    line = "<Name_RelationType>" + objRelItem.Name_RelationType + "</Name_RelationType>";
                                    textWriter.WriteLine(line);
                                    textWriter2.WriteLine(line);
                                    line = "<OrderID>" + objRelItem.OrderID + "</OrderID>";
                                    textWriter.WriteLine(line);
                                    textWriter2.WriteLine(line);
                                    line = "<Exist>1</Exist>";
                                    textWriter.WriteLine(line);
                                    textWriter2.WriteLine(line);
                                    line = "</tmptbl>";
                                    textWriter.WriteLine(line);
                                    textWriter2.WriteLine(line);
                                }
                                else
                                {
                                    break;
                                }

                                line = "</root>";
                                textWriter.WriteLine(line);
                                textWriter2.WriteLine(line);
                                textWriter.Close();
                                textWriter2.Close();

                                if (!string.IsNullOrEmpty(classNameRight))
                                {
                                    command = new SqlCommand("create_Table_relT", connection);
                                    command.Parameters.Add("@Ontology", SqlDbType.NVarChar).Value = localConfig.Globals.Type_ObjectRel;
                                    command.Parameters.Add("@Name_Class_Left", SqlDbType.NVarChar).Value = classNameLeft;
                                    command.Parameters.Add("@Name_Class_Right", SqlDbType.NVarChar).Value = classNameRight;
                                    command.Parameters.Add("@Name_RelationType", SqlDbType.NVarChar).Value = relationTypeName;
                                    command.Parameters.Add("@Path_File", SqlDbType.NVarChar).Value = path;
                                    command.Parameters.Add("@Objects", SqlDbType.Bit).Value = true;
                                    command.ExecuteNonQuery();

                                }

                                command = new SqlCommand("create_Table_relT_OR", connection);
                                command.Parameters.Add("@Ontology", SqlDbType.NVarChar).Value = localConfig.Globals.Type_Other;
                                command.Parameters.Add("@Name_Class_Left", SqlDbType.NVarChar).Value = classNameLeft;
                                command.Parameters.Add("@Name_RelationType", SqlDbType.NVarChar).Value = relationTypeName;
                                command.Parameters.Add("@Path_File", SqlDbType.NVarChar).Value = path2;
                                command.Parameters.Add("@Objects", SqlDbType.Bit).Value = true;
                                command.ExecuteNonQuery();
                                

                                ix = jx;
                            }


                        }
                    }
                    else
                    {
                        var classSearchLeft = new List<clsOntologyItem>
                        {
                            new clsOntologyItem
                            {
                                GUID = classRel.ID_Class_Left
                            }
                        };

                        var classSearchRight = new List<clsOntologyItem>
                        {
                            new clsOntologyItem
                            {
                                GUID = classRel.ID_Class_Right
                            }
                        };

                        var relSearch = new List<clsOntologyItem>
                        {
                            new clsOntologyItem
                            {
                                GUID = classRel.ID_RelationType
                            }
                        };

                        dbReaderClasses.GetDataClasses(classSearchLeft);
                        dbReaderClasses.GetDataClasses(classSearchRight, true);
                        dbReaderRelType.GetDataRelationTypes(relSearch);

                        if (dbReaderClasses.Classes1.Any() &&
                            dbReaderClasses.Classes2.Any() &&
                            dbReaderRelType.RelationTypes.Any())
                        {
                            if (!string.IsNullOrEmpty(classNameRight))
                            {
                                command = new SqlCommand("create_Table_relT", connection);
                                command.Parameters.Add("@Ontology", SqlDbType.NVarChar).Value = localConfig.Globals.Type_ObjectRel;
                                command.Parameters.Add("@Name_Class_Left", SqlDbType.NVarChar).Value = classNameLeft;
                                command.Parameters.Add("@Name_Class_Right", SqlDbType.NVarChar).Value = classNameRight;
                                command.Parameters.Add("@Name_RelationType", SqlDbType.NVarChar).Value = relationTypeName;
                                command.Parameters.Add("@Path_File", SqlDbType.NVarChar).Value = path;
                                command.Parameters.Add("@Objects", SqlDbType.Bit).Value = false;
                                command.ExecuteNonQuery();

                                
                            }

                            command = new SqlCommand("create_Table_relT_OR", connection);
                            command.Parameters.Add("@Ontology", SqlDbType.NVarChar).Value = localConfig.Globals.Type_Other;
                            command.Parameters.Add("@Name_Class_Left", SqlDbType.NVarChar).Value = classNameLeft;
                            command.Parameters.Add("@Name_RelationType", SqlDbType.NVarChar).Value = relationTypeName;
                            command.Parameters.Add("@Path_File", SqlDbType.NVarChar).Value = path2;
                            command.Parameters.Add("@Objects", SqlDbType.Bit).Value = false;
                            command.ExecuteNonQuery();

                        }
                    }

                    command = new SqlCommand("finalize_Table_relT", connection);
                    command.Parameters.Add("@Name_Class_Left", SqlDbType.NVarChar).Value = classNameLeft;
                    command.Parameters.Add("@Name_Class_Right", SqlDbType.NVarChar).Value = classNameRight;
                    command.Parameters.Add("@Name_RelationType", SqlDbType.NVarChar).Value = relationTypeName;
                    command.ExecuteNonQuery();

                    command = new SqlCommand("finalize_Table_relT_OR", connection);
                    command.Parameters.Add("@Name_Class_Left", SqlDbType.NVarChar).Value = classNameLeft;
                    command.Parameters.Add("@Name_RelationType", SqlDbType.NVarChar).Value = relationTypeName;
                    command.ExecuteNonQuery();
                }
                
                if (finalize)
                {

                    var command = new SqlCommand("finalize_Tables", connection);
                    command.Parameters.Add("@Type", SqlDbType.NVarChar).Value = localConfig.Globals.Type_ObjectRel;
                    command.ExecuteNonQuery();
                }


            }


            GC.Collect();

            return result;
        }

       
        private void GetDataAsync(object param)
        {
            var paramItem = (ThreadParam)param;

            var nextRows = new List<Dictionary<string, object>>();

            DataSet dataSet = new DataSet();
            var dataAdapter = new OleDbDataAdapter(paramItem.Query, paramItem.ConnectionString);

            dataAdapter.Fill(dataSet);

            var dataTable = dataSet.Tables[0];

            var count = 0;

            var columns = dataTable.Columns.Cast<DataColumn>().ToList();

            var fields = (from col in columns
                          join repField in paramItem.ReportFields on col.ColumnName equals repField.NameCol
                          select repField).ToList();

            dataTable.Rows.Cast<DataRow>().ToList().ForEach(row =>
            {
                var dict = new Dictionary<string, object>();
                dict.Add("IdRow", Guid.NewGuid().ToString());

                fields.ForEach(repField =>
                {
                    dict.Add(repField.NameCol, row[repField.NameCol]);
                });
               
                nextRows.Add(dict);
                //if (count > 0 && (count % 10 == 0) )
                //{
                //    NextRows = nextRows;
                //    nextRows = new List<Dictionary<string, object>>();
                //}
                //else
                //{
                //    nextRows.Add(dict);
                //}
                count++;
            });

            if (nextRows.Any())
            {
                NextRows = nextRows;
            }
            
        }

        public void StopRead()
        {
            StopReadData();
        }

        public void StopReadData()
        {
            if (getNextRowsAsync != null)
            {
                try
                {
                    getNextRowsAsync.Abort();
                }
                catch(Exception ex)
                {

                }
            }
            lngOffset = 0;
        }



        public ServiceAgent_MSSQL(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;
        }

    }

    class ThreadParam
    {
        public string ConnectionString { get; set; }
        public string Query { get; set; }
        public List<ReportField> ReportFields { get; set; }
    }
}
