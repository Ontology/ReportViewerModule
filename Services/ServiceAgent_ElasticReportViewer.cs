﻿using ImportExport_Module.Base;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using ReportViewerModule.Notifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ReportViewerModule.Services
{
    public class ServiceAgent_ElasticReportViewer : NotifyPropertyChange
    {
        private clsLocalConfig localConfig;

        private object asyncLocker = new object();

        private Thread getReportFieldsAsync;

        #region DataProperties
        private List<clsObjectRel> reportToReportType;
        public List<clsObjectRel> ReportToReportType
        {
            get
            {
                lock(asyncLocker)
                {
                    return reportToReportType;
                }
                
            }
            private set
            {
                lock(asyncLocker)
                {
                    reportToReportType = value;
                }
                RaisePropertyChanged(NotifyChanges.Elastic_ReportToReportType);
            }
        }

        private List<clsObjectRel> reportFieldsOfReport;
        public List<clsObjectRel> ReportFieldsOfReport
        {
            get
            {
                lock (asyncLocker)
                {
                    return reportFieldsOfReport;
                }

            }
            private set
            {
                lock (asyncLocker)
                {
                    reportFieldsOfReport = value;
                }
                RaisePropertyChanged(NotifyChanges.Elastic_ReportFieldsOfReport);
            }
        }

        
        private List<clsObjectRel> reportToView;
        public List<clsObjectRel> ReportToView
        {
            get
            {
                lock (asyncLocker)
                {
                    return reportToView;
                }

            }
            private set
            {
                lock (asyncLocker)
                {
                    reportToView = value;
                }
                RaisePropertyChanged(NotifyChanges.Elastic_ReportToView);
            }
        }

        private List<clsObjectAtt> reportFieldsToInvisible;
        public List<clsObjectAtt> ReportFieldsToInvisible
        {
            get
            {
                lock (asyncLocker)
                {
                    return reportFieldsToInvisible;
                }

            }
            private set
            {
                lock (asyncLocker)
                {
                    reportFieldsToInvisible = value;
                }
                RaisePropertyChanged(NotifyChanges.Elastic_ReportFieldsToInvisible);
            }
        }

        private List<clsObjectRel> reportFieldsToColumns;
        public List<clsObjectRel> ReportFieldsToColumns
        {
            get
            {
                lock (asyncLocker)
                {
                    return reportFieldsToColumns;
                }

            }
            private set
            {
                lock (asyncLocker)
                {
                    reportFieldsToColumns = value;
                }
                RaisePropertyChanged(NotifyChanges.Elastic_ReportFieldsToColumns);
            }
        }

        private List<clsObjectRel> columnsToViews;
        public List<clsObjectRel> ColumnsToViews
        {
            get
            {
                lock (asyncLocker)
                {
                    return columnsToViews;
                }

            }
            private set
            {
                lock (asyncLocker)
                {
                    columnsToViews = value;
                }
                RaisePropertyChanged(NotifyChanges.Elastic_ColumnsToViews);
            }
        }

        private List<clsObjectRel> dBOnServerToViews;
        public List<clsObjectRel> DBOnServerToViews
        {
            get
            {
                lock (asyncLocker)
                {
                    return dBOnServerToViews;
                }

            }
            private set
            {
                lock (asyncLocker)
                {
                    dBOnServerToViews = value;
                }
                RaisePropertyChanged(NotifyChanges.Elastic_DBOnServerToViews);
            }
        }

        
        private List<clsObjectRel> leadFieldsToFields;
        public List<clsObjectRel> LeadFieldsToFields
        {
            get
            {
                lock (asyncLocker)
                {
                    return leadFieldsToFields;
                }

            }
            private set
            {
                lock (asyncLocker)
                {
                    leadFieldsToFields = value;
                }
                RaisePropertyChanged(NotifyChanges.Elastic_LeadFieldsToFields);
            }
        }

        private List<clsObjectRel> dBOnServerToDatabase;
        public List<clsObjectRel> DBOnServerToDatabase
        {
            get
            {
                lock (asyncLocker)
                {
                    return dBOnServerToDatabase;
                }

            }
            private set
            {
                lock (asyncLocker)
                {
                    dBOnServerToDatabase = value;
                }
                RaisePropertyChanged(NotifyChanges.Elastic_DBOnServerToDatabase);
            }
        }


        private List<clsObjectRel> dBOnServerToServer;
        public List<clsObjectRel> DBOnServerToServer
        {
            get
            {
                lock (asyncLocker)
                {
                    return dBOnServerToServer;
                }

            }
            private set
            {
                lock (asyncLocker)
                {
                    dBOnServerToServer = value;
                }
                RaisePropertyChanged(NotifyChanges.Elastic_DBOnServerToServer);
            }
        }

        private List<clsObjectRel> typeFieldsToFields;
        public List<clsObjectRel> TypeFieldsToFields
        {
            get
            {
                lock (asyncLocker)
                {
                    return typeFieldsToFields;
                }

            }
            private set
            {
                lock (asyncLocker)
                {
                    typeFieldsToFields = value;
                }
                RaisePropertyChanged(NotifyChanges.Elastic_TypeFieldsToFields);
            }
        }

        
        private List<clsObjectRel> fieldsToFieldTypes;
        public List<clsObjectRel> FieldsToFieldTypes
        {
            get
            {
                lock (asyncLocker)
                {
                    return fieldsToFieldTypes;
                }

            }
            private set
            {
                lock (asyncLocker)
                {
                    fieldsToFieldTypes = value;
                }
                RaisePropertyChanged(NotifyChanges.Elastic_FieldsToFieldTypes);
            }
        }

        
        private List<clsObjectRel> fieldsToFieldFormats;
        public List<clsObjectRel> FieldsToFieldFormats
        {
            get
            {
                lock (asyncLocker)
                {
                    return fieldsToFieldFormats;
                }

            }
            private set
            {
                lock (asyncLocker)
                {
                    fieldsToFieldFormats = value;
                }
                RaisePropertyChanged(NotifyChanges.Elastic_FieldsToFieldTypes);
            }
        }

        private List<clsObjectRel> aggregatsOfReport;
        public List<clsObjectRel> AggregatsOfReport
        {
            get
            {
                lock(asyncLocker)
                {
                    return aggregatsOfReport;
                }
            }
            set
            {
                lock (asyncLocker)
                {
                    aggregatsOfReport = value;
                }
                RaisePropertyChanged(nameof(AggregatsOfReport));
            }
        }

        private List<clsObjectRel> aggregateTypesOfAggregates;
        public List<clsObjectRel> AggregateTypesOfAggregates
        {
            get
            {
                lock (asyncLocker)
                {
                    return aggregateTypesOfAggregates;
                }
            }
            set
            {
                lock (asyncLocker)
                {
                    aggregateTypesOfAggregates = value;
                }
                RaisePropertyChanged(nameof(AggregateTypesOfAggregates));
            }
        }

        private List<clsObjectRel> aggregateFields;
        public List<clsObjectRel> AggregateFields
        {
            get
            {
                lock (asyncLocker)
                {
                    return aggregateFields;
                }
            }
            set
            {
                lock (asyncLocker)
                {
                    aggregateFields = value;
                }
                RaisePropertyChanged(nameof(AggregateFields));
            }
        }


        #endregion

        private clsOntologyItem resultReportTypes;
        public clsOntologyItem ResultReportTypes
        {
            get { return resultReportTypes; }
            set
            {
                resultReportTypes = value;
                RaisePropertyChanged(NotifyChanges.Elastic_ResultReportTypes);
            }
        }

        public clsOntologyItem GetOItem(string id, string type)
        {
            var dbReader = new OntologyModDBConnector(localConfig.Globals);
            return dbReader.GetOItem(id, type);
        }

        public clsOntologyItem GetReportFields(clsOntologyItem oItemReportOrRef)
        {
            StopReadReportFieldsAsync();

            getReportFieldsAsync = new Thread(GetReportFieldsAsync);
            getReportFieldsAsync.Start(oItemReportOrRef);

            return localConfig.Globals.LState_Success.Clone();
        }

        private ResultGetReport GetReport(clsOntologyItem oItemRef)
        {
            var resultGetReport = new ResultGetReport
            {
                Result = localConfig.Globals.LState_Success.Clone()
            };
            var searchReportLeftRight = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Object = oItemRef.GUID,
                    ID_Parent_Other = localConfig.OItem_type_reports.GUID
                }
            };

            var dbReaderLeftRight = new OntologyModDBConnector(localConfig.Globals);

            var result = dbReaderLeftRight.GetDataObjectRel(searchReportLeftRight);
            resultGetReport.Result = result;
            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                return resultGetReport;
            }

            if (dbReaderLeftRight.ObjectRels.Any())
            {
                resultGetReport.OItemReport = dbReaderLeftRight.ObjectRels.Select(rel => new clsOntologyItem
                {
                    GUID = rel.ID_Other,
                    Name = rel.Name_Other,
                    GUID_Parent = rel.ID_Parent_Other,
                    Type = localConfig.Globals.Type_Object
                }).First();

                return resultGetReport;
            }


            var searchReportRightLeft = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Other = oItemRef.GUID,
                    ID_Parent_Object = localConfig.OItem_type_reports.GUID
                }
            };

            var dbReaderRightLeft = new OntologyModDBConnector(localConfig.Globals);

            result = dbReaderRightLeft.GetDataObjectRel(searchReportRightLeft);

            resultGetReport.Result = result;
            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                return resultGetReport;
            }

            if (!dbReaderRightLeft.ObjectRels.Any())
            {
                resultGetReport.Result = localConfig.Globals.LState_Nothing.Clone();
                return resultGetReport;
            };

            resultGetReport.OItemReport = dbReaderRightLeft.ObjectRels.Select(rel => new clsOntologyItem
            {
                GUID = rel.ID_Object,
                Name = rel.Name_Object,
                GUID_Parent = rel.ID_Parent_Object,
                Type = localConfig.Globals.Type_Object
            }).First();

            return resultGetReport;
        }

        private void GetReportFieldsAsync(object item)
        {
            var result = localConfig.Globals.LState_Success.Clone();
            var oItemReport = (clsOntologyItem)item;
            if (oItemReport.GUID_Parent != localConfig.OItem_type_reports.GUID)
            {
                var resultGetReport = GetReport(oItemReport);
                result = resultGetReport.Result;
                if (result.GUID == localConfig.Globals.LState_Error.GUID || result.GUID == localConfig.Globals.LState_Nothing.GUID)
                {
                    ResultReportTypes = result;
                    return;
                }
                oItemReport = resultGetReport.OItemReport;
            }
            

            result = GetReportType(oItemReport);
            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ResultReportTypes = result;
                return;
            }

            result = Get_001_MSSQL_ReportFieldsOfReport(oItemReport);
            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ResultReportTypes = result;
                return;
            }

            result = Get_002_MSSQL_ReportToView(oItemReport);
            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ResultReportTypes = result;
                return;
            }

            result = Get_003_MSSQL_ReportFieldsToInvisible(ReportFieldsOfReport);
            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ResultReportTypes = result;
                return;
            }

            result = Get_004_MSSQL_ReportFieldsToColumn(ReportFieldsOfReport);
            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ResultReportTypes = result;
                return;
            }

            result = Get_005_MSSQL_ColumnToView(ReportFieldsToColumns);
            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ResultReportTypes = result;
                return;
            }

            result = Get_006_MSSQL_DatabaseOnServerToViews(ReportToView);
            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ResultReportTypes = result;
                return;
            }

            result = Get_007_MSSQL_DBOnServerToDatabase(DBOnServerToViews);
            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ResultReportTypes = result;
                return;
            }

            result = Get_008_MSSQL_DBOnServerToServer(DBOnServerToViews);
            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ResultReportTypes = result;
                return;
            }

            result = Get_009_MSSQL_LeadFieldsToFields(ReportFieldsOfReport);
            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ResultReportTypes = result;
                return;
            }

            result = Get_010_MSSQL_TypeFieldToFields(ReportFieldsOfReport);
            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ResultReportTypes = result;
                return;
            }

            result = Get_011_MSSQL_FieldsToFieldTypes(ReportFieldsOfReport);
            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ResultReportTypes = result;
                return;
            }

            result = Get_012_MSSQL_FieldsToFieldFormats(ReportFieldsOfReport);
            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ResultReportTypes = result;
                return;
            }

            result = Get_013_ReportToAggregate(oItemReport);
            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ResultReportTypes = result;
                return;
            }

            result = Get_014_AggregateTypes(AggregatsOfReport);
            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ResultReportTypes = result;
                return;
            }

            result = Get_015_AggregateFields(AggregatsOfReport);
            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ResultReportTypes = result;
                return;
            }

            ResultReportTypes = result;
        }

        public void StopRead()
        {
            StopReadReportFieldsAsync();
        }

        private void StopReadReportFieldsAsync()
        {
            if (getReportFieldsAsync != null)
            {
                try
                {
                    getReportFieldsAsync.Abort();
                }
                catch(Exception ex)
                {

                }
            }
        }

        private clsOntologyItem GetReportType(clsOntologyItem oItemReport)
        {
            var dbReader = new OntologyModDBConnector(localConfig.Globals);

            var searchReportType = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Object = oItemReport.GUID,
                    ID_Parent_Other = localConfig.OItem_type_report_type.GUID,
                    ID_RelationType = localConfig.OItem_relationtype_is_of_type.GUID
                }
            };

            var result = dbReader.GetDataObjectRel(searchReportType);

            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                ReportToReportType = dbReader.ObjectRels;
            }
            else
            {
                ReportToReportType = null;
            }

            return result;
        }

        private clsOntologyItem Get_001_MSSQL_ReportFieldsOfReport(clsOntologyItem oItemReport)
        {
            var dbReaderFields = new OntologyModDBConnector(localConfig.Globals);

            var searchReportFieldsOfReport = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Other = oItemReport.GUID,
                    ID_Parent_Object = localConfig.OItem_type_report_field.GUID
                }
            };

            var result = dbReaderFields.GetDataObjectRel(searchReportFieldsOfReport);

            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ReportFieldsOfReport = null;
                return localConfig.Globals.LState_Error.Clone();
            }

            ReportFieldsOfReport = dbReaderFields.ObjectRels.OrderBy(field => field.OrderID).ToList();

            return result;


        }

        private clsOntologyItem Get_002_MSSQL_ReportToView(clsOntologyItem oItemReport)
        {
            var dbReader = new OntologyModDBConnector(localConfig.Globals);

            var searchView = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Object = oItemReport.GUID,
                    ID_Parent_Other = localConfig.OItem_type_db_views.GUID,
                    ID_RelationType = localConfig.OItem_relationtype_is.GUID
                }
            };

            var result = dbReader.GetDataObjectRel(searchView);
            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                ReportToView = dbReader.ObjectRels;
            }
            else
            {
                ReportToView = null;
            }

            return result;
        }

        private clsOntologyItem Get_003_MSSQL_ReportFieldsToInvisible(List<clsObjectRel> reportFieldsOfReport)
        {
            var dbReaderInvisible = new OntologyModDBConnector(localConfig.Globals);
            var searchInvisibleAttributes = reportFieldsOfReport.Select(field => new clsObjectAtt
            {
                ID_Object = field.ID_Object,
                ID_AttributeType = localConfig.OItem_attribute_invisible.GUID
            }).ToList();

            var result = localConfig.Globals.LState_Success.Clone();
            if (searchInvisibleAttributes.Any())
            {
                result = dbReaderInvisible.GetDataObjectAtt(searchInvisibleAttributes);

                if (result.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    ReportFieldsToInvisible = null;
                    return localConfig.Globals.LState_Error.Clone();
                }

                ReportFieldsToInvisible = dbReaderInvisible.ObjAtts;
            }
            else
            {
                ReportFieldsToInvisible = new List<clsObjectAtt>();
            }

            return result;
        }

        private clsOntologyItem Get_004_MSSQL_ReportFieldsToColumn(List<clsObjectRel> reportFieldsOfReport)
        {
            var dbReader = new OntologyModDBConnector(localConfig.Globals);
            var searchColumns = reportFieldsOfReport.Select(field => new clsObjectRel
            {
                ID_Object = field.ID_Object,
                ID_RelationType = localConfig.OItem_relationtype_belongsto.GUID,
                ID_Parent_Other = localConfig.OItem_type_db_columns.GUID
            }).ToList();

            var result = localConfig.Globals.LState_Success.Clone();
            if (searchColumns.Any())
            {
                result = dbReader.GetDataObjectRel(searchColumns);
                if (result.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    ReportFieldsToColumns = null;
                }
                else
                {
                    ReportFieldsToColumns = dbReader.ObjectRels;
                }

            }
            else
            {
                ReportFieldsToColumns = new List<clsObjectRel>();
            }

            return result;
            

            
        }

        private clsOntologyItem Get_005_MSSQL_ColumnToView(List<clsObjectRel> reportFieldsToColumns)
        {
            var dbReader = new OntologyModDBConnector(localConfig.Globals);

            var searchView = reportFieldsToColumns.Select(column => new clsObjectRel
            {
                ID_Object = column.ID_Other,
                ID_RelationType = localConfig.OItem_relationtype_belongsto.GUID,
                ID_Parent_Other = localConfig.OItem_type_db_views.GUID
            }).ToList();

            var result = localConfig.Globals.LState_Success.Clone();
            if (searchView.Any())
            {
                result = dbReader.GetDataObjectRel(searchView);

                if (result.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    ColumnsToViews = null;
                }
                else
                {
                    ColumnsToViews = dbReader.ObjectRels;
                }
            }
            else
            {
                ColumnsToViews = new List<clsObjectRel>();
            }

            return result;
        }
        
        private clsOntologyItem Get_006_MSSQL_DatabaseOnServerToViews(List<clsObjectRel> reportToView)
        {
            var dbReader = new OntologyModDBConnector(localConfig.Globals);

            var searchDatabaseOnServer = reportToView.Select(view => new clsObjectRel
            {
                ID_Other = view.ID_Other,
                ID_RelationType = localConfig.OItem_relationtype_contains.GUID,
                ID_Parent_Object = localConfig.OItem_type_database_on_server.GUID
            }).ToList();

            var result = localConfig.Globals.LState_Success.Clone();
            if (searchDatabaseOnServer.Any())
            {
                result = dbReader.GetDataObjectRel(searchDatabaseOnServer);

                if (result.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    DBOnServerToViews = null;
                }
                else
                {
                    DBOnServerToViews = dbReader.ObjectRels;
                }
            }
            else
            {
                DBOnServerToViews = new List<clsObjectRel>();
            }

            return result;
        }

        private clsOntologyItem Get_007_MSSQL_DBOnServerToDatabase(List<clsObjectRel> dbOnServerToColumns)
        {
            var dbReader = new OntologyModDBConnector(localConfig.Globals);

            var searchDBOnServerToDatabase = dbOnServerToColumns.Select(dbOnServer => new clsObjectRel
            {
                ID_Object = dbOnServer.ID_Object,
                ID_RelationType = localConfig.OItem_relationtype_belongsto.GUID,
                ID_Parent_Other = localConfig.OItem_type_database.GUID
            }).ToList();

            var result = localConfig.Globals.LState_Success.Clone();
            if (searchDBOnServerToDatabase.Any())
            {
                result = dbReader.GetDataObjectRel(searchDBOnServerToDatabase);

                if (result.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    DBOnServerToDatabase = null;
                }
                else
                {
                    DBOnServerToDatabase = dbReader.ObjectRels;
                }
            }
            else
            {
                DBOnServerToDatabase = new List<clsObjectRel>();
            }

            return result;
        }

        private clsOntologyItem Get_008_MSSQL_DBOnServerToServer(List<clsObjectRel> dbOnServerToColumns)
        {
            var dbReader = new OntologyModDBConnector(localConfig.Globals);

            var searchDBOnServerToServer = dbOnServerToColumns.Select(dbOnServer => new clsObjectRel
            {
                ID_Object = dbOnServer.ID_Object,
                ID_RelationType = localConfig.OItem_relationtype_located_in.GUID,
                ID_Parent_Other = localConfig.OItem_type_server.GUID
            }).ToList();

            var result = localConfig.Globals.LState_Success.Clone();
            if (searchDBOnServerToServer.Any())
            {
                result = dbReader.GetDataObjectRel(searchDBOnServerToServer);

                if (result.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    DBOnServerToServer = null;
                }
                else
                {
                    DBOnServerToServer = dbReader.ObjectRels;
                }
            }
            else
            {
                DBOnServerToServer = new List<clsObjectRel>();
            }

            return result;
        }

        private clsOntologyItem Get_009_MSSQL_LeadFieldsToFields(List<clsObjectRel> reportFieldsOfReport)
        {
            var dbReader = new OntologyModDBConnector(localConfig.Globals);

            var searchFieldsToLeadFields = reportFieldsOfReport.Select(reportFieldOfReport => new clsObjectRel
            {
                ID_Other = reportFieldOfReport.ID_Object,
                ID_RelationType = localConfig.OItem_relationtype_leads.GUID,
                ID_Parent_Object = localConfig.OItem_type_report_field.GUID
            }).ToList();

            var result = localConfig.Globals.LState_Success.Clone();
            if (searchFieldsToLeadFields.Any())
            {
                result = dbReader.GetDataObjectRel(searchFieldsToLeadFields);

                if (result.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    LeadFieldsToFields = null;
                }
                else
                {
                    LeadFieldsToFields = dbReader.ObjectRels;
                }
            }
            else
            {
                LeadFieldsToFields = new List<clsObjectRel>();
            }

            return result;
        }

        private clsOntologyItem Get_010_MSSQL_TypeFieldToFields(List<clsObjectRel> reportFieldsOfReport)
        {
            var dbReader = new OntologyModDBConnector(localConfig.Globals);

            var searchTypeFieldsToFields = reportFieldsOfReport.Select(reportFieldOfReport => new clsObjectRel
            {
                ID_Other = reportFieldOfReport.ID_Object,
                ID_RelationType = localConfig.OItem_relationtype_type_field.GUID,
                ID_Parent_Object = localConfig.OItem_type_report_field.GUID
            }).ToList();

            var result = localConfig.Globals.LState_Success.Clone();
            if (searchTypeFieldsToFields.Any())
            {
                result = dbReader.GetDataObjectRel(searchTypeFieldsToFields);

                if (result.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    TypeFieldsToFields = null;
                }
                else
                {
                    TypeFieldsToFields = dbReader.ObjectRels;
                }
            }
            else
            {
                TypeFieldsToFields = new List<clsObjectRel>();
            }

            return result;
        }

        private clsOntologyItem Get_011_MSSQL_FieldsToFieldTypes(List<clsObjectRel> reportFieldsOfReport)
        {
            var dbReader = new OntologyModDBConnector(localConfig.Globals);

            var searchFieldsToFieldTypes = reportFieldsOfReport.Select(reportFieldOfReport => new clsObjectRel
            {
                ID_Object = reportFieldOfReport.ID_Object,
                ID_RelationType = localConfig.OItem_relationtype_is_of_type.GUID,
                ID_Parent_Other = localConfig.OItem_type_field_type.GUID
            }).ToList();

            var result = localConfig.Globals.LState_Success.Clone();
            if (searchFieldsToFieldTypes.Any())
            {
                result = dbReader.GetDataObjectRel(searchFieldsToFieldTypes);

                if (result.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    FieldsToFieldTypes = null;
                }
                else
                {
                    FieldsToFieldTypes = dbReader.ObjectRels;
                }
            }
            else
            {
                FieldsToFieldTypes = new List<clsObjectRel>();
            }

            return result;
        }

        private clsOntologyItem Get_012_MSSQL_FieldsToFieldFormats(List<clsObjectRel> reportFieldsOfReport)
        {
            var dbReader = new OntologyModDBConnector(localConfig.Globals);

            var searchFieldsToFieldFormats = reportFieldsOfReport.Select(reportFieldOfReport => new clsObjectRel
            {
                ID_Object = reportFieldOfReport.ID_Object,
                ID_RelationType = localConfig.OItem_relationtype_formatted_by.GUID,
                ID_Parent_Other = localConfig.OItem_type_field_format.GUID
            }).ToList();

            var result = localConfig.Globals.LState_Success.Clone();
            if (searchFieldsToFieldFormats.Any())
            {
                result = dbReader.GetDataObjectRel(searchFieldsToFieldFormats);

                if (result.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    FieldsToFieldFormats = null;
                }
                else
                {
                    FieldsToFieldFormats = dbReader.ObjectRels;
                }
            }
            else
            {
                FieldsToFieldFormats = new List<clsObjectRel>();
            }

            return result;
        }

        private clsOntologyItem Get_013_ReportToAggregate(clsOntologyItem oItemReport)
        {
            var dbReaderAggregates = new OntologyModDBConnector(localConfig.Globals);

            var aggregatesOfReports = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Other = oItemReport.GUID,
                    ID_RelationType = localConfig.OItem_relationtype_belongsto.GUID,
                    ID_Parent_Object = localConfig.OItem_class_aggregate.GUID
                }
            };

            var result = dbReaderAggregates.GetDataObjectRel(aggregatesOfReports);

            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ReportFieldsOfReport = null;
                return localConfig.Globals.LState_Error.Clone();
            }

            AggregatsOfReport = dbReaderAggregates.ObjectRels.OrderBy(field => field.OrderID).ToList();

            return result;



        }

        private clsOntologyItem Get_014_AggregateTypes(List<clsObjectRel> aggregatesOfReport)
        {
            var dbReader = new OntologyModDBConnector(localConfig.Globals);

            var searchAggregateTypes = aggregatesOfReport.Select(aggregate => new clsObjectRel
            {
                ID_Object = aggregate.ID_Object,
                ID_RelationType = localConfig.OItem_relationtype_is_of_type.GUID,
                ID_Parent_Other = localConfig.OItem_class_aggregate_type.GUID
            }).ToList();

            var result = localConfig.Globals.LState_Success.Clone();
            if (searchAggregateTypes.Any())
            {
                result = dbReader.GetDataObjectRel(searchAggregateTypes);

                if (result.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    AggregateTypesOfAggregates = null;
                }
                else
                {
                    AggregateTypesOfAggregates = dbReader.ObjectRels;
                }
            }
            else
            {
                AggregateTypesOfAggregates = new List<clsObjectRel>();
            }

            return result;
        }

        private clsOntologyItem Get_015_AggregateFields(List<clsObjectRel> aggregatesOfReport)
        {
            var dbReader = new OntologyModDBConnector(localConfig.Globals);

            var searchAggregateFields = aggregatesOfReport.Select(aggregate => new clsObjectRel
            {
                ID_Object = aggregate.ID_Object,
                ID_RelationType = localConfig.OItem_relationtype_belongsto.GUID,
                ID_Parent_Other = localConfig.OItem_type_report_field.GUID
            }).ToList();

            var result = localConfig.Globals.LState_Success.Clone();
            if (searchAggregateFields.Any())
            {
                result = dbReader.GetDataObjectRel(searchAggregateFields);

                if (result.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    AggregateFields = null;
                }
                else
                {
                    AggregateFields = dbReader.ObjectRels;
                }
            }
            else
            {
                AggregateFields = new List<clsObjectRel>();
            }

            return result;
        }



        public ServiceAgent_ElasticReportViewer(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;
        }
    }

    public class ResultGetReport
    {
        public clsOntologyItem Result { get; set; }
        public clsOntologyItem OItemReport { get; set; }
    }
}
