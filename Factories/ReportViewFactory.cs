﻿using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Base;
using ReportViewerModule.Models;
using ReportViewerModule.Notifications;
using ReportViewerModule.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ReportViewerModule.Factories
{
    public class ReportViewFactory: NotifyPropertyChange
    {
        private clsLocalConfig localConfig;


        private object asyncLocker = new object();

        private List<ReportField> reportFields;
        public List<ReportField> ReportFields
        {
            get
            {
                lock(asyncLocker)
                {
                    return reportFields;
                }
                
            }
            private set
            {
                lock (asyncLocker)
                {
                    reportFields = value;
                }
                RaisePropertyChanged(NotifyChanges.ReportFieldsFactory_ReportFields);
            }
        }

        private ReportItem report;
        public ReportItem Report
        {
            get
            {
                lock(asyncLocker)
                {
                    return report;
                }
            }
            private set
            {
                lock(asyncLocker)
                {
                    report = value;
                }
                RaisePropertyChanged(NotifyChanges.ReportFieldsFactory_Report);
            }
        }

        private Thread createReportFieldListAsync;
        private Thread createReportAsync;

        public void StopCreate()
        {
            StopCreateReportFields();
            StopCreateReport();

        }

        private void StopCreateReportFields()
        {
            if (createReportFieldListAsync != null)
            {
                try
                {
                    createReportFieldListAsync.Abort();
                }
                catch (Exception ex)
                {

                }
            }
        }

        private void StopCreateReport()
        {
            if (createReportAsync != null)
            {
                try
                {
                    createReportAsync.Abort();
                }
                catch(Exception ex)
                {

                }
            }
        }

        public clsOntologyItem CreateReportFieldList(List<clsObjectRel> reportFieldsOfReport,
            List<clsObjectRel> reportToView,
            List<clsObjectAtt> reportFieldsToInvisible,
            List<clsObjectRel> reportFieldsToColumns,
            List<clsObjectRel> columnsToViews,
            List<clsObjectRel> dBOnServerToViews,
            List<clsObjectRel> leadFieldsToFields,
            List<clsObjectRel> dBOnServerToDatabase,
            List<clsObjectRel> dBOnServerToServer,
            List<clsObjectRel> typeFieldsToFields,
            List<clsObjectRel> fieldsToFieldTypes,
            List<clsObjectRel> fieldsToFieldFormats,
            List<clsObjectRel> aggregatesOfReport,
            List<clsObjectRel> aggregateTypes,
            List<clsObjectRel> aggregateFields) 
        {
            var threadParam = new ThreadParamFields
            {
                ReportFieldsOfReport = reportFieldsOfReport,
                ReportToView = reportToView,
                ReportFieldsToInvisible = reportFieldsToInvisible,
                ReportFieldsToColumns =  reportFieldsToColumns,
                ColumnsToViews = columnsToViews,
                DBOnServerToViews = dBOnServerToViews,
                LeadFieldsToFields = leadFieldsToFields,
                DBOnServerToDatabase = dBOnServerToDatabase,
                DBOnServerToServer = dBOnServerToServer,
                TypeFieldsToFields = typeFieldsToFields,
                FieldsToFieldTypes = fieldsToFieldTypes,
                FieldsToFieldFormats = fieldsToFieldFormats,
                AggregatesOfReport = aggregatesOfReport,
                AggregateTypes = aggregateTypes,
                AggregateFields = aggregateFields
            };

            StopCreateReportFields();

            createReportFieldListAsync = new Thread(CreateReportFieldListAsync);
            createReportFieldListAsync.Start(threadParam);

            return localConfig.Globals.LState_Success.Clone();
        }

        private void CreateReportFieldListAsync(object param)
        {
            var threadParam = (ThreadParamFields)param;

            var colsOfFields = (from fieldRel in threadParam.ReportFieldsOfReport
                                join colRel in threadParam.ReportFieldsToColumns on fieldRel.ID_Object equals colRel.ID_Object
                                select new
                                {
                                    IdField = fieldRel.ID_Object,
                                    IdCol = colRel.ID_Other,
                                    NameCol = colRel.Name_Other
                                });

            var dbViewsOfCols = (from colRel in colsOfFields
                                 join dbViewOfColRel in threadParam.ColumnsToViews on colRel.IdCol equals dbViewOfColRel.ID_Object
                                 select new
                                 {
                                     IdField = colRel.IdField,
                                     IdCol = colRel.IdCol,
                                     IdView = dbViewOfColRel.ID_Other,
                                     NameView = dbViewOfColRel.Name_Other
                                 });

            var dbViewsOfReport = (from fieldRel in threadParam.ReportFieldsOfReport
                                   join viewRel in threadParam.ReportToView on fieldRel.ID_Other equals viewRel.ID_Object
                                   select new
                                   {
                                       IdReport = fieldRel.ID_Other,
                                       IdView = viewRel.ID_Other,
                                       NameView = viewRel.Name_Other
                                   }).GroupBy(view => new { IdReport = view.IdReport, IdView = view.IdView, NameView = view.NameView });

            var dbOnServersOfReports = (from viewRel in dbViewsOfReport
                                       join dbOnServerRel in threadParam.DBOnServerToViews on viewRel.Key.IdView equals dbOnServerRel.ID_Other
                                       select new
                                       {
                                           IdReport = viewRel.Key.IdReport,
                                           IdView = viewRel.Key.IdView,
                                           IdDBOnServer = dbOnServerRel.ID_Object,
                                           NameDBOnServer = dbOnServerRel.Name_Object
                                       });

            var serverOfReports = (from dbOnServerRel in dbOnServersOfReports
                                   join serverRel in threadParam.DBOnServerToServer on dbOnServerRel.IdDBOnServer equals serverRel.ID_Object
                                  select new
                                  {
                                      IdField = dbOnServerRel.IdView,
                                      IdView = dbOnServerRel.IdView,
                                      IdDBOnServer = dbOnServerRel.IdDBOnServer,
                                      IdServer = serverRel.ID_Other,
                                      NameServer = serverRel.Name_Other
                                  });

            var databaseOfReports = (from dbOnServerRel in dbOnServersOfReports
                                     join databaseRel in threadParam.DBOnServerToDatabase on dbOnServerRel.IdDBOnServer equals databaseRel.ID_Object
                                  select new
                                  {
                                      IdField = dbOnServerRel.IdView,
                                      IdView = dbOnServerRel.IdView,
                                      IdDBOnServer = dbOnServerRel.IdDBOnServer,
                                      IdDatabase = databaseRel.ID_Other,
                                      NameDatabase = databaseRel.Name_Other
                                  });

            var fieldTypesOfFields = (from fieldRel in threadParam.ReportFieldsOfReport
                                      join fieldType in threadParam.FieldsToFieldTypes on fieldRel.ID_Object equals fieldType.ID_Object
                                      select new
                                      {
                                          IdField = fieldRel.ID_Object,
                                          IdFieldType = fieldType.ID_Other,
                                          NameFieldType = fieldType.Name_Other
                                      });

            var fieldFormatsOfFields = (from fieldRel in threadParam.ReportFieldsOfReport
                                      join fieldFormat in threadParam.FieldsToFieldFormats on fieldRel.ID_Object equals fieldFormat.ID_Object
                                      select new
                                      {
                                          IdField = fieldRel.ID_Object,
                                          IdFieldFormat = fieldFormat.ID_Other,
                                          NameFieldFormat = fieldFormat.Name_Other
                                      });

            var leadFieldsOfFields = (from fieldRel in threadParam.ReportFieldsOfReport
                                      join leadFieldRel in threadParam.LeadFieldsToFields on fieldRel.ID_Object equals leadFieldRel.ID_Other
                                      select new
                                      {
                                          IdField = fieldRel.ID_Object,
                                          IdLeadField = leadFieldRel.ID_Object,
                                          NameLeadField = leadFieldRel.Name_Object
                                      });

            var typeFieldsOfFields = (from fieldRel in threadParam.ReportFieldsOfReport
                                      join typeFieldRel in threadParam.TypeFieldsToFields on fieldRel.ID_Object equals typeFieldRel.ID_Object
                                      join leadFieldRel in leadFieldsOfFields on fieldRel.ID_Object equals leadFieldRel.IdField
                                      where typeFieldRel.ID_RelationType == localConfig.OItem_relationtype_type_field.GUID
                                        && fieldRel.ID_RelationType == localConfig.OItem_relationtype_belongsto.GUID
                                      select new
                                      {
                                          IdField = fieldRel.ID_Object,
                                          IdTypeField = typeFieldRel.ID_Other,
                                          NameTypeField = typeFieldRel.Name_Other
                                      });

            var aggregates = (from aggregate in threadParam.AggregatesOfReport
                              join aggregateField in threadParam.AggregateFields on aggregate.ID_Object equals aggregateField.ID_Object
                              join aggregateType in threadParam.AggregateTypes on aggregateField.ID_Object equals aggregateType.ID_Object
                              select new
                              {
                                  IdField = aggregateField.ID_Other,
                                  IdAggregateType = aggregateType.ID_Other,
                                  AggregateType = aggregateType.Name_Other
                              });

            var reportFields = (from fieldRel in threadParam.ReportFieldsOfReport
                                join inVisibility in threadParam.ReportFieldsToInvisible on fieldRel.ID_Object equals inVisibility.ID_Object
                                join viewRel in dbViewsOfReport on fieldRel.ID_Other equals viewRel.Key.IdReport
                                join colRel in colsOfFields on fieldRel.ID_Object equals colRel.IdField
                                join colViewrel in dbViewsOfCols on new { IdCol = colRel.IdCol, IdView = viewRel.Key.IdView } equals new { IdCol = colViewrel.IdCol, IdView = colViewrel.IdView }
                                join dbOnServerRel in dbOnServersOfReports on fieldRel.ID_Other equals dbOnServerRel.IdReport
                                join databaseRel in databaseOfReports on dbOnServerRel.IdDBOnServer equals databaseRel.IdDBOnServer
                                join serverRel in serverOfReports on dbOnServerRel.IdDBOnServer equals serverRel.IdDBOnServer
                                join fieldType in fieldTypesOfFields on fieldRel.ID_Object equals fieldType.IdField
                                join fieldFormat in fieldFormatsOfFields on fieldRel.ID_Object equals fieldFormat.IdField into fieldFormats
                                from fieldFormat in fieldFormats.DefaultIfEmpty()
                                join leadField in leadFieldsOfFields on fieldRel.ID_Object equals leadField.IdField into leadFields
                                from leadField in leadFields.DefaultIfEmpty()
                                join typeField in typeFieldsOfFields on fieldRel.ID_Object equals typeField.IdField into typeFields
                                from typeField in typeFields.DefaultIfEmpty()
                                join aggregate in aggregates on fieldRel.ID_Object equals aggregate.IdField into aggregates1
                                from aggregate in aggregates1.DefaultIfEmpty()
                                select new ReportField
                                {
                                    IdReport = fieldRel.ID_Other,
                                    NameReport = fieldRel.Name_Other,
                                    IdReportField = fieldRel.ID_Object,
                                    NameReportField = fieldRel.Name_Object,
                                    IdAttributeVisible = inVisibility.ID_Attribute,
                                    IsVisible = inVisibility.Val_Bit.Value,
                                    IdCol = colRel.IdCol,
                                    NameCol = colRel.NameCol,
                                    IdDatabase = databaseRel.IdDatabase,
                                    NameDatabase = databaseRel.NameDatabase,
                                    IdFieldType = fieldType.IdFieldType,
                                    NameFieldType = fieldType.NameFieldType,
                                    IdDBOnServer = dbOnServerRel.IdDBOnServer,
                                    NameDBOnServer = dbOnServerRel.NameDBOnServer,
                                    IdDBView = colViewrel.IdView,
                                    NameDBView = colViewrel.NameView,
                                    IdFieldFormat = fieldFormat != null ? fieldFormat.IdFieldFormat : null,
                                    NameFieldFormat = fieldFormat != null ? fieldFormat.NameFieldFormat : null,
                                    IdLeadField = leadField != null ? leadField.IdLeadField : null,
                                    NameLeadField = leadField != null ? leadField.NameLeadField : null,
                                    IdRow = Guid.NewGuid().ToString(),
                                    IdServer = serverRel.IdServer,
                                    NameServer = serverRel.NameServer,
                                    IdTypeField = typeField != null ? typeField.IdTypeField : null,
                                    NameTypeField = typeField != null ? typeField.NameTypeField : null,
                                    IdView = viewRel.Key.IdView,
                                    NameView = viewRel.Key.NameView,
                                    OrderId = fieldRel.OrderID.Value,
                                    IdAggregateType = aggregate != null ? aggregate.IdAggregateType : null,
                                    AggregateType = aggregate != null ? aggregate.AggregateType : null
                                }).ToList();

            ReportFields = reportFields;
        }

        public ReportViewFactory(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;
        }

        public clsOntologyItem GetReport(clsOntologyItem oItemReportOrRef,
            List<clsObjectRel> reportToReportType,
            List<clsObjectRel> reportToView,
            List<clsObjectRel> dbOnServersToViews,
            List<clsObjectRel> dbOnServersToDatabases,
            List<clsObjectRel> dbOnServersToServers)
        {

            var threadParam = new ThreadParamReport
            {
                OItemReportOrRef = oItemReportOrRef,
                ReportToReportType = reportToReportType,
                ReportToView = reportToView,
                DBOnServerToViews = dbOnServersToViews,
                DBOnServerToDatabase = dbOnServersToDatabases,
                DBOnServerToServer = dbOnServersToServers
            };

            StopCreateReport();

            createReportAsync = new Thread(GetReportAsync);
            createReportAsync.Start(threadParam);

            return localConfig.Globals.LState_Success.Clone();
        }

        private ResultGetReport GetReport(clsOntologyItem oItemRef)
        {
            var resultGetReport = new ResultGetReport
            {
                Result = localConfig.Globals.LState_Success.Clone()
            };
            var searchReportLeftRight = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Object = oItemRef.GUID,
                    ID_Parent_Other = localConfig.OItem_type_reports.GUID
                }
            };

            var dbReaderLeftRight = new OntologyModDBConnector(localConfig.Globals);

            var result = dbReaderLeftRight.GetDataObjectRel(searchReportLeftRight);
            resultGetReport.Result = result;
            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                return resultGetReport;
            }

            if (dbReaderLeftRight.ObjectRels.Any())
            {
                resultGetReport.OItemReport = dbReaderLeftRight.ObjectRels.Select(rel => new clsOntologyItem
                {
                    GUID = rel.ID_Other,
                    Name = rel.Name_Other,
                    GUID_Parent = rel.ID_Parent_Other,
                    Type = localConfig.Globals.Type_Object
                }).First();

                return resultGetReport;
            }


            var searchReportRightLeft = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Other = oItemRef.GUID,
                    ID_Parent_Object = localConfig.OItem_type_reports.GUID
                }
            };

            var dbReaderRightLeft = new OntologyModDBConnector(localConfig.Globals);

            result = dbReaderRightLeft.GetDataObjectRel(searchReportRightLeft);

            resultGetReport.Result = result;
            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                return resultGetReport;
            }

            if (!dbReaderRightLeft.ObjectRels.Any())
            {
                resultGetReport.Result = localConfig.Globals.LState_Nothing.Clone();
                return resultGetReport;
            };

            resultGetReport.OItemReport = dbReaderRightLeft.ObjectRels.Select(rel => new clsOntologyItem
            {
                GUID = rel.ID_Object,
                Name = rel.Name_Object,
                GUID_Parent = rel.ID_Parent_Object,
                Type = localConfig.Globals.Type_Object
            }).First();

            return resultGetReport;
        }

        private void GetReportAsync(object threadParam)
        {
            var paramItem = (ThreadParamReport)threadParam;

            var oItemReport = paramItem.OItemReportOrRef;
            if (paramItem.OItemReportOrRef.GUID_Parent != localConfig.OItem_type_reports.GUID)
            {
                var result = GetReport(paramItem.OItemReportOrRef);
                if (result.Result.GUID == localConfig.Globals.LState_Error.GUID || result.Result.GUID == localConfig.Globals.LState_Nothing.GUID)
                {
                    Report = null;
                    return;
                }
                oItemReport = result.OItemReport;
            }

            var reports = (from reportToView in paramItem.ReportToView.Where(rep => rep.ID_Object == oItemReport.GUID)
                           join reportType in paramItem.ReportToReportType on reportToView.ID_Object equals reportType.ID_Object
                           join dbOnServerToView in paramItem.DBOnServerToViews on reportToView.ID_Other equals dbOnServerToView.ID_Other
                           join dbOnServerToDatabase in paramItem.DBOnServerToDatabase on dbOnServerToView.ID_Object equals dbOnServerToDatabase.ID_Object
                           join dbOnServerToServer in paramItem.DBOnServerToServer on dbOnServerToView.ID_Object equals dbOnServerToServer.ID_Object
                           select new ReportItem
                           {
                               IdReport = reportToView.ID_Object,
                               NameReport = reportToView.Name_Object,
                               IdReportType = reportType.ID_Other,
                               NameReportType = reportType.Name_Other,
                               IdDBViewOrEsType = dbOnServerToView.ID_Other,
                               NameDBViewOrEsType = dbOnServerToView.Name_Other,
                               IdDatabaseOnServer = dbOnServerToView.ID_Object,
                               NameDatabaseOnServer = dbOnServerToView.Name_Object,
                               IdServer = dbOnServerToServer.ID_Other,
                               NameServer = dbOnServerToServer.Name_Other,
                               IdDatabaseOrIndex = dbOnServerToDatabase.ID_Other,
                               NameDatabaseOrIndex = dbOnServerToDatabase.Name_Other,
                               IdRow = Guid.NewGuid().ToString()
                           }).ToList();

            Report = reports.FirstOrDefault();
        }
    }


    class ThreadParamFields
    {
        public List<clsObjectRel> ReportFieldsOfReport { get; set; }
        public List<clsObjectRel> ReportToView { get; set; }
        public List<clsObjectAtt> ReportFieldsToInvisible { get; set; }
        public List<clsObjectRel> ReportFieldsToColumns { get; set; }
        public List<clsObjectRel> ColumnsToViews { get; set; }
        public List<clsObjectRel> DBOnServerToViews { get; set; }
        public List<clsObjectRel> LeadFieldsToFields { get; set; }
        public List<clsObjectRel> DBOnServerToDatabase { get; set; }
        public List<clsObjectRel> DBOnServerToServer { get; set; }
        public List<clsObjectRel> TypeFieldsToFields { get; set; }
        public List<clsObjectRel> FieldsToFieldTypes { get; set; }
        public List<clsObjectRel> FieldsToFieldFormats { get; set; }
        public List<clsObjectRel> AggregatesOfReport { get; set; }
        public List<clsObjectRel> AggregateFields { get; set; }
        public List<clsObjectRel> AggregateTypes { get; set; }
    }

    class ThreadParamReport
    {
        public clsOntologyItem OItemReportOrRef { get; set; }
        public List<clsObjectRel> ReportToReportType { get; set; }
        public List<clsObjectRel> ReportToView { get; set; }
        public List<clsObjectRel> DBOnServerToViews { get; set; }
        public List<clsObjectRel> DBOnServerToDatabase { get; set; }
        public List<clsObjectRel> DBOnServerToServer { get; set; }
    }
}
