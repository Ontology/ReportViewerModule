﻿using OntoMsg_Module.Models;
using OntoWebCore.Attributes;
using OntoWebCore.Converter;
using OntoWebCore.Models;
using ReportViewerModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportViewerModule.Factories
{
    public class ReportGridFactory
    {
        private clsLocalConfig localConfig;

        public Dictionary<string, object> CreateGridConfig(List<ReportField> reportFields, SessionFile sessionFile)
        {

            using (var stream = sessionFile.StreamWriter)
            {
                using (var jsonStream = new Newtonsoft.Json.JsonTextWriter(stream))
                {
                    jsonStream.WriteStartArray();
                    //reportFields.ForEach(reportField =>
                    //{
                    //    jsonStream.WriteStartObject();
                    //    jsonStream.WritePropertyName("field");
                    //    jsonStream.WriteValue(reportField.NameCol);
                    //    jsonStream.WriteRawValue(null);
                    //    jsonStream.WriteEndObject();
                    //});
                    jsonStream.WriteEndArray();
                }
            }

                var gridConfig = new Dictionary<string, object>();

            gridConfig.Add("groupable", true);
            gridConfig.Add("sortable", true);
            gridConfig.Add("autobind", false);
            gridConfig.Add("height", "100%");
            gridConfig.Add("scrollable", true);
            gridConfig.Add("resizable", true);
            gridConfig.Add("selectable", "multiple cell");
            gridConfig.Add("allowCopy", true);
            gridConfig.Add("columnMenu", true);

            var pageConfig = new Dictionary<string, object>();
            pageConfig.Add("refresh", true);
            
            var pageSizes = new List<object>
            {
                5,10,50,100,1000
            };
            pageConfig.Add("pageSizes", pageSizes);
            pageConfig.Add("numeric", false);
    
            var messagesConfig = new Dictionary<string, object>();
            messagesConfig.Add("display", "Showing {0}-{1} from {2} data items");

            pageConfig.Add("messages", messagesConfig);

            gridConfig.Add("pageable", pageConfig);

            var filterConfig = new Dictionary<string, object>();
            filterConfig.Add("extra", false);

            var operatorsConfig = new Dictionary<string, object>();
            var stringConfig = new Dictionary<string, object>();
            stringConfig.Add("contains", "Contains");
            stringConfig.Add("startswith", "Starts with");
            stringConfig.Add("eq", "Is equal to");
            stringConfig.Add("neq", "Is not equal to");
            operatorsConfig.Add("string", stringConfig);

            filterConfig.Add("operators", operatorsConfig);

            gridConfig.Add("filterable", filterConfig);


            var columnList = reportFields.Select(field => {
                var columnAttribute = new KendoColumnAttribute()
                {
                    field = field.NameCol,
                    hidden = field.IsVisible,
                    title = field.NameReportField,
                    Order = (int)field.OrderId,
                    width = "150px",
                    filterable = true
                };

                if (field.NameFieldType == "DateTime")
                {
                    columnAttribute.type = field.NameFieldType.ToLower();
                    columnAttribute.template = "#=kendo.toString(kendo.parseDate(" + field.NameCol + ", 'yyyy-MM-ddTHH:mm:ss'),'" + field.NameFieldFormat + "')#";
                }

                if (!string.IsNullOrEmpty(field.NameFieldFormat))
                {
                    columnAttribute.format = "{0:" + field.NameFieldFormat + "}";
                }
                if (!string.IsNullOrEmpty(field.AggregateType))
                {
                    columnAttribute.aggregates = new string[] { field.AggregateType };
                    if (!string.IsNullOrEmpty(field.NameFieldFormat))
                    {
                        columnAttribute.footerTemplate = field.AggregateType + ": #=kendo.toString(" + field.AggregateType + ",'" + field.NameFieldFormat + "') #";
                    }
                    else
                    {
                        columnAttribute.footerTemplate = field.AggregateType + ": #=" + field.AggregateType + "#";
                    }
                        
                }
                return columnAttribute;

            }).OrderBy(field => field.Order).ToList();
            gridConfig.Add("columns", columnList);
            var dataSourceConfig = new Dictionary<string, object>();
            var transportConfig = new Dictionary<string, object>();
            var readConfig = new Dictionary<string, object>();
            readConfig.Add("url", sessionFile.FileUri.AbsoluteUri);
            readConfig.Add("dataType", "json");
            transportConfig.Add("read", readConfig);
            dataSourceConfig.Add("transport", transportConfig);
            dataSourceConfig.Add("pageSize", 20);

            var aggregates = reportFields.Where(reportField => !string.IsNullOrEmpty(reportField.AggregateType)).ToList();

            if (aggregates.Any())
            {
                var dictAggregate = new List<Dictionary<string, object>>();

                dataSourceConfig.Add("aggregate", dictAggregate);

                aggregates.ForEach(aggregate =>
                {
                    var dictSingleAggregate = new Dictionary<string, object>();

                    dictSingleAggregate.Add("field", aggregate.NameCol);
                    dictSingleAggregate.Add("aggregate", aggregate.AggregateType);
                    dictAggregate.Add(dictSingleAggregate);
                });
            }

            gridConfig.Add("dataSource", dataSourceConfig);

            

            return gridConfig;
        }

        public ReportGridFactory(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;
        }
    }
}
