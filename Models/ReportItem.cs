﻿using OntologyAppDBConnector.Base;
using OntoMsg_Module.Base;
using OntoWebCore.Attributes;
using ReportViewerModule.Notifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportViewerModule.Models
{
    public class ReportItem : NotifyPropertyChange
    {
        private string idRow;
        [Json()]
		[DataViewColumn(CellType = CellType.String, IsIdField = true, IsVisible = false)]
        public string IdRow
        {
            get { return idRow; }
            set
            {
                if (idRow == value) return;

                idRow = value;

                RaisePropertyChanged(NotifyChanges.DataModel_IdRow);

            }
        }

        private string idReport;
        [Json()]
		[DataViewColumn(CellType = CellType.String, IsIdField = false, IsVisible = false)]
        public string IdReport
        {
            get { return idReport; }
            set
            {
                if (idReport == value) return;

                idReport = value;

                RaisePropertyChanged(NotifyChanges.DataModel_IdReport);

            }
        }

        private string nameReport;
        [Json()]
		[DataViewColumn(Caption = "Report", CellType = CellType.String, DisplayOrder = 0, IsIdField = false, IsVisible = true)]
        public string NameReport
        {
            get { return nameReport; }
            set
            {
                if (nameReport == value) return;

                nameReport = value;

                RaisePropertyChanged(NotifyChanges.DataModel_NameReport);

            }
        }

        private string idReportType;
        [Json()]
		[DataViewColumn(CellType = CellType.String, IsIdField = false, IsVisible = false)]
        public string IdReportType
        {
            get { return idReportType; }
            set
            {
                if (idReportType == value) return;

                idReportType = value;

                RaisePropertyChanged(NotifyChanges.DataModel_IdReportType);

            }
        }

        private string nameReportType;
        [Json()]
		[DataViewColumn(Caption = "Reporttype", CellType = CellType.String, DisplayOrder = 1, IsIdField = false, IsVisible = true)]
        public string NameReportType
        {
            get { return nameReportType; }
            set
            {
                if (nameReportType == value) return;

                nameReportType = value;

                RaisePropertyChanged(NotifyChanges.DataModel_NameReportType);

            }
        }

        private string idDatabaseOnServer;
        [Json()]
		[DataViewColumn(CellType = CellType.String, IsIdField = false, IsVisible = false)]
        public string IdDatabaseOnServer
        {
            get { return idDatabaseOnServer; }
            set
            {
                if (idDatabaseOnServer == value) return;

                idDatabaseOnServer = value;

                RaisePropertyChanged(NotifyChanges.DataModel_IdDatabaseOnServer);

            }
        }

        private string nameDatabaseOnServer;
        [Json()]
		[DataViewColumn(CellType = CellType.String, IsIdField = false, IsVisible = false)]
        public string NameDatabaseOnServer
        {
            get { return nameDatabaseOnServer; }
            set
            {
                if (nameDatabaseOnServer == value) return;

                nameDatabaseOnServer = value;

                RaisePropertyChanged(NotifyChanges.DataModel_NameDatabaseOnServer);

            }
        }

        private string idDBViewOrEsType;
        [Json()]
		[DataViewColumn(CellType = CellType.String, IsIdField = false, IsVisible = false)]
        public string IdDBViewOrEsType
        {
            get { return idDBViewOrEsType; }
            set
            {
                if (idDBViewOrEsType == value) return;

                idDBViewOrEsType = value;

                RaisePropertyChanged(NotifyChanges.DataModel_IdDBViewOrEsType);

            }
        }

        private string nameDBViewOrEsType;
        [Json()]
		[DataViewColumn(Caption = "View", CellType = CellType.String, DisplayOrder = 2, IsIdField = false, IsVisible = true)]
        public string NameDBViewOrEsType
        {
            get { return nameDBViewOrEsType; }
            set
            {
                if (nameDBViewOrEsType == value) return;

                nameDBViewOrEsType = value;

                RaisePropertyChanged(NotifyChanges.DataModel_NameDBViewOrEsType);

            }
        }

        private string idDatabaseOrIndex;
        [Json()]
		[DataViewColumn(CellType = CellType.String, IsIdField = false, IsVisible = false)]
        public string IdDatabaseOrIndex
        {
            get { return idDatabaseOrIndex; }
            set
            {
                if (idDatabaseOrIndex == value) return;

                idDatabaseOrIndex = value;

                RaisePropertyChanged(NotifyChanges.DataModel_IdDatabaseOrIndex);

            }
        }

        private string nameDatabaseOrIndex;
        [Json()]
		[DataViewColumn(Caption = "Database", CellType = CellType.String, DisplayOrder = 3, IsIdField = false, IsVisible = true)]
        public string NameDatabaseOrIndex
        {
            get { return nameDatabaseOrIndex; }
            set
            {
                if (nameDatabaseOrIndex == value) return;

                nameDatabaseOrIndex = value;

                RaisePropertyChanged(NotifyChanges.DataModel_NameDatabaseOrIndex);

            }
        }

        private string idServer;
        [Json()]
		[DataViewColumn(CellType = CellType.String, IsIdField = false, IsVisible = false)]
        public string IdServer
        {
            get { return idServer; }
            set
            {
                if (idServer == value) return;

                idServer = value;

                RaisePropertyChanged(NotifyChanges.DataModel_IdServer);

            }
        }

        private string nameServer;
        [Json()]
		[DataViewColumn(Caption = "Server", CellType = CellType.String, DisplayOrder = 4, IsIdField = false, IsVisible = true)]
        public string NameServer
        {
            get { return nameServer; }
            set
            {
                if (nameServer == value) return;

                nameServer = value;

                RaisePropertyChanged(NotifyChanges.DataModel_NameServer);

            }
        }

        private string idPort;
        [Json()]
		[DataViewColumn(CellType = CellType.String, IsIdField = false, IsVisible = false)]
        public string IdPort
        {
            get { return idPort; }
            set
            {
                if (idPort == value) return;

                idPort = value;

                RaisePropertyChanged(NotifyChanges.DataModel_IdPort);

            }
        }

        private string namePort;
        [Json()]
		[DataViewColumn(CellType = CellType.String, IsIdField = false, IsVisible = false)]
        public string NamePort
        {
            get { return namePort; }
            set
            {
                if (namePort == value) return;

                namePort = value;

                long tmpPort;
                if  (long.TryParse(namePort, out tmpPort))
                {
                    Port = tmpPort;
                }
                else
                {
                    Port = null;
                }

                RaisePropertyChanged(NotifyChanges.DataModel_NamePort);

            }
        }

        private long? port;
        [Json()]
		[DataViewColumn(Caption = "Port", CellType = CellType.Integer, DisplayOrder = 5, IsIdField = false, IsVisible = true)]
        public long? Port
        {
            get { return port; }
            set
            {
                if (port == value) return;

                port = value;

                RaisePropertyChanged(NotifyChanges.DataModel_Port);

            }
        }
    }
}
