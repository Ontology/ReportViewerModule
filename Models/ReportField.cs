﻿using OntologyAppDBConnector.Base;
using OntoMsg_Module.Base;
using OntoWebCore.Attributes;
using ReportViewerModule.Notifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportViewerModule.Models
{
    public class ReportField : NotifyPropertyChange
    {
        private string idRow;
        [Json()]
		[DataViewColumn(CellType = CellType.String, IsIdField = true, IsVisible = false)]
        public string IdRow
        {
            get { return idRow; }
            set
            {
                if (idRow == value) return;

                idRow = value;

                RaisePropertyChanged(NotifyChanges.DataModel_IdRow);

            }
        }

        private string idReport;
        [Json()]
		[DataViewColumn(CellType = CellType.String, IsIdField = false, IsVisible = false)]
        public string IdReport
        {
            get { return idReport; }
            set
            {
                if (idReport == value) return;

                idReport = value;

                RaisePropertyChanged(NotifyChanges.DataModel_IdReport);

            }
        }

        private string nameReport;
        [Json()]
		[DataViewColumn(Caption = "Report", CellType = CellType.String, DisplayOrder = 0, IsIdField = false, IsVisible = true)]
        public string NameReport
        {
            get { return nameReport; }
            set
            {
                if (nameReport == value) return;

                nameReport = value;

                RaisePropertyChanged(NotifyChanges.DataModel_NameReport);

            }
        }

        private string idReportField;
        [Json()]
		[DataViewColumn(CellType = CellType.String, IsIdField = false, IsVisible = false)]
        public string IdReportField
        {
            get { return idReportField; }
            set
            {
                if (idReportField == value) return;

                idReportField = value;

                RaisePropertyChanged(NotifyChanges.DataModel_IdReportField);

            }
        }

        private string nameReportField;
        [Json()]
		[DataViewColumn(Caption = "Report-Field", CellType = CellType.String, DisplayOrder = 1, IsIdField = false, IsVisible = true)]
        public string NameReportField
        {
            get { return nameReportField; }
            set
            {
                if (nameReportField == value) return;

                nameReportField = value;

                RaisePropertyChanged(NotifyChanges.DataModel_NameReportField);

            }
        }

        private bool isVisible;
        [Json()]
        [DataViewColumn(Caption = "Visible", CellType = CellType.Boolean, DisplayOrder = 2, IsIdField = false, IsVisible = true)]
        public bool IsVisible
        {
            get { return isVisible; }
            set
            {
                if (isVisible == value) return;

                isVisible = value;

                RaisePropertyChanged(NotifyChanges.DataModel_IsVisible);

            }
        }

        private string idAttributeVisible;
        [Json()]
        [DataViewColumn(CellType = CellType.String, IsIdField = false, IsVisible = false)]
        public string IdAttributeVisible
        {
            get { return idAttributeVisible; }
            set
            {
                if (idAttributeVisible == value) return;

                idAttributeVisible = value;

                RaisePropertyChanged(NotifyChanges.DataModel_IdAttributeVisible);

            }
        }

        private string idParseField;
        [Json()]
		[DataViewColumn(CellType = CellType.String, IsIdField = false, IsVisible = false)]
        public string IdParseField
        {
            get { return idParseField; }
            set
            {
                if (idParseField == value) return;

                idParseField = value;

                RaisePropertyChanged(NotifyChanges.DataModel_IdParseField);

            }
        }

        private string nameParseField;
        [Json()]
		[DataViewColumn(Caption = "Parse-Field", CellType = CellType.String, DisplayOrder = 2, IsIdField = false, IsVisible = true)]
        public string NameParseField
        {
            get { return nameParseField; }
            set
            {
                if (nameParseField == value) return;

                nameParseField = value;

                RaisePropertyChanged(NotifyChanges.DataModel_NameParseField);

            }
        }

        private string idDataType;
        [Json()]
		[DataViewColumn(CellType = CellType.String, IsIdField = false, IsVisible = false)]
        public string IdDataType
        {
            get { return idDataType; }
            set
            {
                if (idDataType == value) return;

                idDataType = value;

                RaisePropertyChanged(NotifyChanges.DataModel_IdDataType);

            }
        }

        private string nameDataType;
        [Json()]
		[DataViewColumn(Caption = "Datatype", CellType = CellType.String, DisplayOrder = 3, IsIdField = false, IsVisible = true)]
        public string NameDataType
        {
            get { return nameDataType; }
            set
            {
                if (nameDataType == value) return;

                nameDataType = value;

                RaisePropertyChanged(NotifyChanges.DataModel_NameDataType);

            }
        }

        private string idDBView;
        [Json()]
		[DataViewColumn(CellType = CellType.String, IsIdField = false, IsVisible = false)]
        public string IdDBView
        {
            get { return idDBView; }
            set
            {
                if (idDBView == value) return;

                idDBView = value;

                RaisePropertyChanged(NotifyChanges.DataModel_IdDBView);

            }
        }

        private string nameDBView;
        [Json()]
		[DataViewColumn(Caption = "View", CellType = CellType.String, DisplayOrder = 4, IsIdField = false, IsVisible = true)]
        public string NameDBView
        {
            get { return nameDBView; }
            set
            {
                if (nameDBView == value) return;

                nameDBView = value;

                RaisePropertyChanged(NotifyChanges.DataModel_NameDBView);

            }
        }

        private string idCol;
        [Json()]
		[DataViewColumn(CellType = CellType.String, IsIdField = false, IsVisible = false)]
        public string IdCol
        {
            get { return idCol; }
            set
            {
                if (idCol == value) return;

                idCol = value;

                RaisePropertyChanged(NotifyChanges.DataModel_IdCol);

            }
        }

        private string nameCol;
        [Json()]
		[DataViewColumn(Caption = "Column", CellType = CellType.String, DisplayOrder = 5, IsIdField = false, IsVisible = true)]
        public string NameCol
        {
            get { return nameCol; }
            set
            {
                if (nameCol == value) return;

                nameCol = value;

                RaisePropertyChanged(NotifyChanges.DataModel_NameCol);

            }
        }

        private string idView;
        [Json()]
		[DataViewColumn(CellType = CellType.String, IsIdField = false, IsVisible = false)]
        public string IdView
        {
            get { return idView; }
            set
            {
                if (idView == value) return;

                idView = value;

                RaisePropertyChanged(NotifyChanges.DataModel_IdView);

            }
        }

        private string nameView;
        [Json()]
		[DataViewColumn(Caption = "View", CellType = CellType.String, DisplayOrder = 6, IsIdField = false, IsVisible = true)]
        public string NameView
        {
            get { return nameView; }
            set
            {
                if (nameView == value) return;

                nameView = value;

                RaisePropertyChanged(NotifyChanges.DataModel_NameView);

            }
        }

        private string idDBOnServer;
        [Json()]
		[DataViewColumn(CellType = CellType.String, IsIdField = false, IsVisible = false)]
        public string IdDBOnServer
        {
            get { return idDBOnServer; }
            set
            {
                if (idDBOnServer == value) return;

                idDBOnServer = value;

                RaisePropertyChanged(NotifyChanges.DataModel_IdDBOnServer);

            }
        }

        private string nameDBOnServer;
        [Json()]
		[DataViewColumn(CellType = CellType.String, IsIdField = false, IsVisible = false)]
        public string NameDBOnServer
        {
            get { return nameDBOnServer; }
            set
            {
                if (nameDBOnServer == value) return;

                nameDBOnServer = value;

                RaisePropertyChanged(NotifyChanges.DataModel_NameDBOnServer);

            }
        }

        private string idDatabase;
        [Json()]
		[DataViewColumn(CellType = CellType.String, DisplayOrder = 7, IsIdField = false, IsVisible = false)]
        public string IdDatabase
        {
            get { return idDatabase; }
            set
            {
                if (idDatabase == value) return;

                idDatabase = value;

                RaisePropertyChanged(NotifyChanges.DataModel_IdDatabase);

            }
        }

        private string nameDatabase;
        [Json()]
		[DataViewColumn(Caption = "Database", CellType = CellType.String, DisplayOrder = 7, IsIdField = false, IsVisible = true)]
        public string NameDatabase
        {
            get { return nameDatabase; }
            set
            {
                if (nameDatabase == value) return;

                nameDatabase = value;

                RaisePropertyChanged(NotifyChanges.DataModel_NameDatabase);

            }
        }

        private string idServer;
        [Json()]
		[DataViewColumn(CellType = CellType.String, IsIdField = false, IsVisible = false)]
        public string IdServer
        {
            get { return idServer; }
            set
            {
                if (idServer == value) return;

                idServer = value;

                RaisePropertyChanged(NotifyChanges.DataModel_IdServer);

            }
        }

        private string nameServer;
        [Json()]
		[DataViewColumn(Caption = "Server", CellType = CellType.String, DisplayOrder = 8, IsIdField = false, IsVisible = true)]
        public string NameServer
        {
            get { return nameServer; }
            set
            {
                if (nameServer == value) return;

                nameServer = value;

                RaisePropertyChanged(NotifyChanges.DataModel_NameServer);

            }
        }

        private string idFieldType;
        [Json()]
		[DataViewColumn(CellType = CellType.String, IsIdField = false, IsVisible = false)]
        public string IdFieldType
        {
            get { return idFieldType; }
            set
            {
                if (idFieldType == value) return;

                idFieldType = value;

                RaisePropertyChanged(NotifyChanges.DataModel_IdFieldType);

            }
        }

        private string nameFieldType;
        [Json()]
		[DataViewColumn(Caption = "Fieldtype", CellType = CellType.String, DisplayOrder = 9, IsIdField = false, IsVisible = true)]
        public string NameFieldType
        {
            get { return nameFieldType; }
            set
            {
                if (nameFieldType == value) return;

                nameFieldType = value;

                RaisePropertyChanged(NotifyChanges.DataModel_NameFieldType);

            }
        }

        private string idFieldFormat;
        [Json()]
		[DataViewColumn(CellType = CellType.String, IsIdField = false, IsVisible = false)]
        public string IdFieldFormat
        {
            get { return idFieldFormat; }
            set
            {
                if (idFieldFormat == value) return;

                idFieldFormat = value;

                RaisePropertyChanged(NotifyChanges.DataModel_IdFieldFormat);

            }
        }

        private string nameFieldFormat;
        [Json()]
		[DataViewColumn(Caption = "Fieldformat", CellType = CellType.String, DisplayOrder = 10, IsIdField = false, IsVisible = true)]
        public string NameFieldFormat
        {
            get { return nameFieldFormat; }
            set
            {
                if (nameFieldFormat == value) return;

                nameFieldFormat = value;

                RaisePropertyChanged(NotifyChanges.DataModel_NameFieldFormat);

            }
        }

        private string idLeadField;
        [Json()]
		[DataViewColumn(CellType = CellType.String, IsIdField = false, IsVisible = false)]
        public string IdLeadField
        {
            get { return idLeadField; }
            set
            {
                if (idLeadField == value) return;

                idLeadField = value;

                RaisePropertyChanged(NotifyChanges.DataModel_IdLeadField);

            }
        }

        private string nameLeadField;
        [Json()]
		[DataViewColumn(Caption = "Leadfield", CellType = CellType.String, DisplayOrder = 11, IsIdField = false, IsVisible = true)]
        public string NameLeadField
        {
            get { return nameLeadField; }
            set
            {
                if (nameLeadField == value) return;

                nameLeadField = value;

                RaisePropertyChanged(NotifyChanges.DataModel_NameLeadField);

            }
        }

        private string idTypeField;
        [Json()]
		[DataViewColumn(CellType = CellType.String, IsIdField = false, IsVisible = false)]
        public string IdTypeField
        {
            get { return idTypeField; }
            set
            {
                if (idTypeField == value) return;

                idTypeField = value;

                RaisePropertyChanged(NotifyChanges.DataModel_IdTypeField);

            }
        }

        private string nameTypeField;
        [Json()]
		[DataViewColumn(Caption = "Typefield", CellType = CellType.String, DisplayOrder = 12, IsIdField = false, IsVisible = true)]
        public string NameTypeField
        {
            get { return nameTypeField; }
            set
            {
                if (nameTypeField == value) return;

                nameTypeField = value;

                RaisePropertyChanged(NotifyChanges.DataModel_NameTypeField);

            }
        }

        private long orderId;
        [Json()]
		[DataViewColumn(Caption = "Order-Id", CellType = CellType.Integer, DisplayOrder = 13, IsIdField = false, IsVisible = true)]
        public long OrderId
        {
            get { return orderId; }
            set
            {
                if (orderId == value) return;

                orderId = value;

                RaisePropertyChanged(NotifyChanges.DataModel_OrderId);

            }
        }

        private string idAggregateType;
        public string IdAggregateType
        {
            get { return idAggregateType; }
            set
            {
                if (idAggregateType == value) return;
                idAggregateType = value;
                RaisePropertyChanged(nameof(IdAggregateType));
            }
        }

        private string aggregateType;
        public string AggregateType
        {
            get { return aggregateType; }
            set
            {
                if (aggregateType == value) return;
                aggregateType = value;
                RaisePropertyChanged(nameof(AggregateType));
            }
        }
    }
}
