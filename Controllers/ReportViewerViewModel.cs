﻿using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoWebCore.Models;
using ReportViewerModule.Notifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportViewerModule.Controllers
{
    public class ReportViewerViewModel : ViewModelBase
    {

        private bool issuccessful_Login;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "loginSuccess", ViewItemType = ViewItemType.Other)]
		public bool IsSuccessful_Login
        {
            get { return issuccessful_Login; }
            set
            {
                if (issuccessful_Login == value) return;

                issuccessful_Login = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsSuccessful_Login);

            }
        }

        private bool istoggled_Listen;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "isListen", ViewItemType = ViewItemType.Other)]
		public bool IsToggled_Listen
        {
            get { return istoggled_Listen; }
            set
            {
                if (istoggled_Listen == value) return;

                istoggled_Listen = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsToggled_Listen);

            }
        }

        private string text_View;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "viewText", ViewItemType = ViewItemType.Other)]
		public string Text_View
        {
            get { return text_View; }
            set
            {
                if (text_View == value) return;

                text_View = value;

                RaisePropertyChanged(NotifyChanges.View_Text_View);

            }
        }

        private Dictionary<string, object> gridConfig;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Grid, ViewItemId = "grid", ViewItemType = ViewItemType.GridConfig)]
		public Dictionary<string,object> GridConfig
        {
            get { return gridConfig; }
            set
            {
                if (gridConfig == value) return;

                gridConfig = value;

                RaisePropertyChanged(nameof(GridConfig));

            }
        }

        private JqxCellItem cellitem_CellDoubleClicked;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Grid, ViewItemId = "grid", ViewItemType = ViewItemType.DoubleClick)]
		public JqxCellItem CellItem_CellDoubleClicked
        {
            get { return cellitem_CellDoubleClicked; }
            set
            {
                if (cellitem_CellDoubleClicked == value) return;

                cellitem_CellDoubleClicked = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_CellItem_CellDoubleClicked);

            }
        }

        private JqxCellItem cellitem_CellSelected;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Grid, ViewItemId = "grid", ViewItemType = ViewItemType.SelectedIndex)]
		public JqxCellItem CellItem_CellSelected
        {
            get { return cellitem_CellSelected; }
            set
            {
                if (cellitem_CellSelected == value) return;

                cellitem_CellSelected = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_CellItem_CellSelected);

            }
        }

        private bool ischecked_OpenUrl;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.ToggleButton, ViewItemId = "openUrl", ViewItemType = ViewItemType.Checked)]
		public bool IsChecked_OpenUrl
        {
            get { return ischecked_OpenUrl; }
            set
            {
                if (ischecked_OpenUrl == value) return;

                ischecked_OpenUrl = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsChecked_OpenUrl);

            }
        }

        private string url_UrlToOpen;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Url, ViewItemId = "urlToOpen", ViewItemType = ViewItemType.Content)]
		public string Url_UrlToOpen
        {
            get { return url_UrlToOpen; }
            set
            {
                if (url_UrlToOpen == value) return;

                url_UrlToOpen = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Url_UrlToOpen);

            }
        }

        private bool isenabled_Sync;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "syncData", ViewItemType = ViewItemType.Enable)]
		public bool IsEnabled_Sync
        {
            get { return isenabled_Sync; }
            set
            {

                isenabled_Sync = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_Sync);

            }
        }

        private bool isenabled_Apply;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "applyItem", ViewItemType = ViewItemType.Enable)]
        public bool Isenabled_Apply
        {
            get { return isenabled_Apply; }
            set
            {

                isenabled_Apply = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_Apply);

            }
        }

        private bool isenabled_Refresh;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "refreshReport", ViewItemType = ViewItemType.Enable)]
		public bool IsEnabled_Refresh
        {
            get { return isenabled_Refresh; }
            set
            {

                isenabled_Refresh = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_Refresh);

            }
        }

        private bool show_Loader;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "jqxLoader", ViewItemType = ViewItemType.Other)]
		public bool Show_Loader
        {
            get { return show_Loader; }
            set
            {
                if (show_Loader == value) return;

                show_Loader = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Show_Loader);

            }
        }
    }
}
