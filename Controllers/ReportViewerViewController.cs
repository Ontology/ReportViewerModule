﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoMsg_Module.Models;
using OntoMsg_Module.Notifications;
using OntoMsg_Module.WebSocketServices;
using OntoWebCore.Factories;
using OntoWebCore.Models;
using ReportViewerModule.Factories;
using ReportViewerModule.Models;
using ReportViewerModule.Services;
using ReportViewerModule.Translations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace ReportViewerModule.Controllers
{
    public class ReportViewerViewController : ReportViewerViewModel, IViewController
    {
        private WebsocketServiceAgent webSocketServiceAgent;

        private TranslationController translationController = new TranslationController();

        private ServiceAgent_ElasticReportViewer serviceAgent_Elastic;
        private ServiceAgent_MSSQL serviceAgentMSSQL;

        private ReportViewFactory reportFieldsFactory;
        private ReportGridFactory reportGridFactory;

        private clsLocalConfig localConfig;

        private clsOntologyItem selectedRef;
        private clsOntologyItem selectedItem;

        private JsonFactory jsonFactory;

        private object nextRowsLocker = new object();

        private List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();

        private List<ReportField> reportFields;
        private ReportItem report;

        private string dataFileName;

        public OntoMsg_Module.StateMachines.IControllerStateMachine StateMachine { get; private set; }
        public OntoMsg_Module.StateMachines.ControllerStateMachine LocalStateMachine
        {
            get
            {
                if (StateMachine == null)
                {
                    return null;
                }

                return (OntoMsg_Module.StateMachines.ControllerStateMachine)StateMachine;
            }
        }

        public ReportViewerViewController()
        {
            // local configuration
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(localConfig);
            }

            Initialize();

            PropertyChanged += ReportViewerViewController_PropertyChanged;
        }

        private void ReportViewerViewController_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var property = ViewModelProperties.FirstOrDefault(viewProperty => viewProperty.Property.Name == e.PropertyName);

            if (property == null) return;

            property.ViewItem.AddValue(property.Property.GetValue(this));

            if (e.PropertyName == Notifications.NotifyChanges.ViewModel_CellItem_CellSelected)
            {
                Isenabled_Apply = false;
                if (CellItem_CellSelected != null)
                {
                    var idRow = CellItem_CellSelected.Id;
                    var row = rows.FirstOrDefault(rowItem => rowItem["IdRow"].ToString() == idRow);
                    if (row == null) return;

                    var column = reportFields.FirstOrDefault(repField => repField.NameCol == CellItem_CellSelected.ColumnName);

                    if (column == null || string.IsNullOrEmpty(column.IdLeadField)) return;

                    var leadField = reportFields.FirstOrDefault(repField => repField.IdReportField == column.IdLeadField);

                    if (leadField != null && row.ContainsKey(leadField.NameCol))
                    {
                        var value = row[leadField.NameCol].ToString();

                        if (localConfig.Globals.is_GUID(value))
                        {
                            var objectItem = serviceAgent_Elastic.GetOItem(value, localConfig.Globals.Type_Object);
                            if (objectItem != null)
                            {
                                selectedItem = objectItem;
                                var interServiceMessage = new InterServiceMessage
                                {
                                    ChannelId = Channels.ParameterList,
                                    OItems = new List<clsOntologyItem> { objectItem }
                                };

                                webSocketServiceAgent.SendInterModMessage(interServiceMessage);
                            }
                        }
                    }

                    if (IsChecked_OpenUrl)
                    {
                        var typeField = reportFields.FirstOrDefault(repField => repField.IdReportField == column.IdTypeField);

                        if (typeField != null && row.ContainsKey(typeField.NameCol))
                        {
                            var value = row[typeField.NameCol].ToString();

                            if (value == localConfig.OItem_type_url.GUID)
                            {
                                Url_UrlToOpen = row[column.NameCol].ToString();
                            }
                        }
                    }
                    
                }
                return;
            }
            else if (e.PropertyName == Notifications.NotifyChanges.ViewModel_CellItem_CellDoubleClicked)
            {
                var idRow = CellItem_CellDoubleClicked.Id;
                var row = rows.FirstOrDefault(rowItem => rowItem["IdRow"].ToString() == idRow);
                if (row == null) return;

                return;
            }

            webSocketServiceAgent.SendPropertyChange(e.PropertyName);
        }

        private void Initialize()
        {
            var stateMachine = new OntoMsg_Module.StateMachines.ControllerStateMachine(OntoMsg_Module.StateMachines.StateMachineType.BlockSelectingSelected | OntoMsg_Module.StateMachines.StateMachineType.BlockLoadingSelected);
            stateMachine.IsControllerListen = true;
            StateMachine = stateMachine;
            stateMachine.PropertyChanged += StateMachine_PropertyChanged;
            stateMachine.loadSelectedItem += StateMachine_loadSelectedItem;
            stateMachine.loginSucceded += StateMachine_loginSucceded;
            stateMachine.openedSocket += StateMachine_openedSocket;
            stateMachine.closedSocket += StateMachine_closedSocket;

            serviceAgent_Elastic = new ServiceAgent_ElasticReportViewer(localConfig);
            serviceAgent_Elastic.PropertyChanged += ServiceAgent_Elastic_PropertyChanged;
            jsonFactory = new JsonFactory();
            reportFieldsFactory = new ReportViewFactory(localConfig);
            reportFieldsFactory.PropertyChanged += ReportFieldsFactory_PropertyChanged;
            serviceAgentMSSQL = new ServiceAgent_MSSQL(localConfig);
            serviceAgentMSSQL.PropertyChanged += ServiceAgentMSSQL_PropertyChanged;
            reportGridFactory = new ReportGridFactory(localConfig);

        }

        private void StateMachine_closedSocket()
        {
            webSocketServiceAgent.RemoveAllResources();
        }

        private void StateMachine_openedSocket()
        {
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Sender,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

        }

        private void StateMachine_loginSucceded()
        {

            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Channels.AppliedObjects,
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Sender,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Channels.ParameterList,
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Receiver,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

            IsToggled_Listen = true;

        }

        private void StateMachine_loadSelectedItem(clsOntologyItem oItemSelected)
        {
            
        }

        private void StateMachine_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(OntoMsg_Module.StateMachines.ControllerStateMachine.LoginSuccessful))
            {
                IsEnabled_Refresh = false;
                IsEnabled_Sync = false;
                IsSuccessful_Login = LocalStateMachine.LoginSuccessful;
               
            }
        }

        private void ServiceAgentMSSQL_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == Notifications.NotifyChanges.ServiceMSSQL_NextRows)
            {
                if (serviceAgentMSSQL.NextRows != null)
                {
                    lock(nextRowsLocker)
                    {
                        rows.AddRange(serviceAgentMSSQL.NextRows);

                        var sessionFile = webSocketServiceAgent.RequestWriteStream(dataFileName);
                        if (sessionFile == null)
                        {
                            System.Threading.Thread.Sleep(500);
                            sessionFile = webSocketServiceAgent.RequestWriteStream(dataFileName);
                        }
                        
                        using (sessionFile.StreamWriter)
                        {
                            var jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(serviceAgentMSSQL.NextRows);
                            sessionFile.StreamWriter.Write(jsonString);
                        }
                        webSocketServiceAgent.SendCommand("NextRows");
                        Show_Loader = false;
                        IsEnabled_Refresh = true;
                    }
                }
            }
        }

        private void ReportFieldsFactory_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == Notifications.NotifyChanges.ReportFieldsFactory_ReportFields)
            {
                if (reportFieldsFactory.ReportFields != null)
                {
                    Text_View = selectedRef.Name;
                    reportFields = reportFieldsFactory.ReportFields;
                    TestReportViewData();
                    
                }
            }
            else if (e.PropertyName == Notifications.NotifyChanges.ReportFieldsFactory_Report)
            {
                if (reportFieldsFactory.Report != null)
                {
                    report = reportFieldsFactory.Report;
                    TestReportViewData();

                }
                else
                {
                    Show_Loader = false;
                }
            }
        }
        
        private void TestReportViewData()
        {
            if  (reportFields != null && report !=null)
            {
                if (report.IdReportType == localConfig.OItem_token_report_type_elasticview.GUID)
                {

                }
                else
                {
                    dataFileName = Guid.NewGuid().ToString() + ".json";
                    var sessionFile = webSocketServiceAgent.RequestWriteStream(dataFileName);

                    if (sessionFile == null) return;

                    GridConfig = reportGridFactory.CreateGridConfig(reportFields, sessionFile);

                    rows.Clear();
                    var result = serviceAgentMSSQL.GetData(report, reportFields);

                }
            }
        }

        private void ServiceAgent_Elastic_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == Notifications.NotifyChanges.Elastic_ResultReportTypes)
            {
                if (serviceAgent_Elastic.ResultReportTypes.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    var result = reportFieldsFactory.CreateReportFieldList(serviceAgent_Elastic.ReportFieldsOfReport,
                        serviceAgent_Elastic.ReportToView,
                        serviceAgent_Elastic.ReportFieldsToInvisible,
                        serviceAgent_Elastic.ReportFieldsToColumns,
                        serviceAgent_Elastic.ColumnsToViews,
                        serviceAgent_Elastic.DBOnServerToViews,
                        serviceAgent_Elastic.LeadFieldsToFields,
                        serviceAgent_Elastic.DBOnServerToDatabase,
                        serviceAgent_Elastic.DBOnServerToServer,
                        serviceAgent_Elastic.TypeFieldsToFields,
                        serviceAgent_Elastic.FieldsToFieldTypes,
                        serviceAgent_Elastic.FieldsToFieldFormats,
                        serviceAgent_Elastic.AggregatsOfReport,
                        serviceAgent_Elastic.AggregateTypesOfAggregates,
                        serviceAgent_Elastic.AggregateFields);

                    result = reportFieldsFactory.GetReport(selectedRef,
                        serviceAgent_Elastic.ReportToReportType,
                        serviceAgent_Elastic.ReportToView,
                        serviceAgent_Elastic.DBOnServerToViews,
                        serviceAgent_Elastic.DBOnServerToDatabase,
                        serviceAgent_Elastic.DBOnServerToServer);
                }
                else if (serviceAgent_Elastic.ResultReportTypes.GUID == localConfig.Globals.LState_Nothing.GUID)
                {
                    Show_Loader = false;
                }
            }
        }

        public void InitializeViewController(WebsocketServiceAgent webSocketServiceAgent)
        {
            this.webSocketServiceAgent = webSocketServiceAgent;
            this.webSocketServiceAgent.PropertyChanged += WebSocketServiceAgent_PropertyChanged;
            this.webSocketServiceAgent.comServerOnMessage += WebSocketServiceAgent_comServerOnMessage;
            this.webSocketServiceAgent.comServerOpened += WebSocketServiceAgent_comServerOpened;


        }

       

        private void WebSocketServiceAgent_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_DataText_SessionId)
            {


            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Command_RequestedCommand)
            {
                if (webSocketServiceAgent.Command_RequestedCommand == "RefreshReport")
                {
                    LoadData();
                }
                if (webSocketServiceAgent.Command_RequestedCommand == "ApplyItem" && selectedItem != null)
                {
                    var appliedRequest = new InterServiceMessage
                    {
                        ChannelId = Channels.AppliedObjects,
                        SenderId = webSocketServiceAgent.EndpointId,
                        OItems = new List<clsOntologyItem>
                        {
                            selectedItem
                        }
                    };

                    webSocketServiceAgent.SendInterModMessage(appliedRequest);
                }

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Event_RequestEvent)
            {

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_ChangedViewItems)
            {
                webSocketServiceAgent.ChangedViewItems.ForEach(changedItem =>
                {
                    if (changedItem.ViewItemId == "grid" && changedItem.ViewItemType == "SelectedIndex")
                    {
                        CellItem_CellSelected = Newtonsoft.Json.JsonConvert.DeserializeObject<JqxCellItem>(changedItem.ViewItemValue.ToString());
                    }
                    else if (changedItem.ViewItemId == "grid" && changedItem.ViewItemType == "DoubleClick")
                    {
                        CellItem_CellDoubleClicked = Newtonsoft.Json.JsonConvert.DeserializeObject<JqxCellItem>(changedItem.ViewItemValue.ToString());

                    }
                });
            }
            else if (e.PropertyName == NotifyChanges.Websocket_ObjectArgument)
            {
                var objectId = webSocketServiceAgent.ObjectArgument.Value.ToString();

                if (string.IsNullOrEmpty(objectId) || !localConfig.Globals.is_GUID(objectId)) return;

                var oItem = serviceAgent_Elastic.GetOItem(objectId, localConfig.Globals.Type_Object);


                if (oItem == null ) return;

                LoadData(oItem);
            }



        }

        private void WebSocketServiceAgent_comServerOpened()
        {
            var authenticationRequest = new InterServiceMessage
            {
                ChannelId = Channels.Login,
                SenderId = webSocketServiceAgent.EndpointId
            };

            webSocketServiceAgent.SendInterModMessage(authenticationRequest);

        }

        private void WebSocketServiceAgent_comServerOnMessage(OntoMsg_Module.Notifications.InterServiceMessage message)
        {

            if (message.ReceiverId != null && message.ReceiverId != webSocketServiceAgent.EndpointId) return;

            if (message.ChannelId == Channels.ReceiverOfDedicatedSender)
            {
            }
            else if (message.ChannelId == Channels.ParameterList)
            {
                


            }

        }

        private void LoadData(clsOntologyItem oItem = null)
        {
            if (selectedRef != null)
            {
                webSocketServiceAgent.SendCommand("DestroyGrid");
            }
            Show_Loader = true;
            IsEnabled_Refresh = false;
            rows = new List<Dictionary<string, object>>();
            reportFields = null;
            report = null;
            if (oItem != null)
            {
                selectedRef = oItem;
            }
            

            var result = serviceAgent_Elastic.GetReportFields(selectedRef);
        }

        public List<ViewModelProperty> GetViewModelProperties(bool onlySend = true, ViewItemType viewItemType = ViewItemType.All, ViewItemClass viewItemClass = ViewItemClass.All)
        {
            return ViewModelProperties.Where(viewItemProp => (onlySend ? viewItemProp.ViewModelAttribute.Send : 1 == 1)
                && (viewItemType != ViewItemType.All ? viewItemProp.ViewModelAttribute.ViewItemType == viewItemType : 1 == 1)
                && (viewItemClass != ViewItemClass.All ? viewItemProp.ViewModelAttribute.ViewItemClass == viewItemClass : 1 == 1)).ToList();
        }
    }
}
